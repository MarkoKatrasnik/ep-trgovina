<?php

// enables sessions for the entire app
session_start();

require_once("anonimniMVC/controller/ShopController.php");
require_once("controller/ShopRESTController.php");
#require_once("strankaMVC/controller/StrankaController.php");
#require_once("prodajalecMVC/controller/ProdajalecController.php");

define("BASE_URL", rtrim($_SERVER["SCRIPT_NAME"], "index.php"));
define("IMAGES_URL", rtrim($_SERVER["SCRIPT_NAME"], "index.php") . "static/images/");
define("CSS_URL", rtrim($_SERVER["SCRIPT_NAME"], "index.php") . "static/css/");

$path = isset($_SERVER["PATH_INFO"]) ? trim($_SERVER["PATH_INFO"], "/") : "";

$urls = [
    "/^items\/?(\d+)?$/" => function ($method, $id = null) {
        if ($id == null) {
            ShopController::index();
        } else {
            ShopController::get($id);
        }
    },
    
    "/^$/" => function () {
        ViewHelper::redirect(BASE_URL . "items");
    },
    
   
            
    # REST API
    
    "/^api\/items$/" => function ($method, $id = null) {
        switch ($method) {
            default: # GET
                ShopRESTController::index();
                break;
        }
    },
    "/^api\/items\/(\d+)$/" => function ($method, $id = null) {
        switch ($method) {
            default: # GET
                ShopRESTController::get($id);
                break;
        }
    },
            
    "/^api\/prijava\/(.+)$/" => function ($method, $email = null) {
        switch ($method) {
            default: # GET
                ShopRESTController::prijava($email);
                break;
        }
    },
            
    "/^api\/profil\/(\d+)$/" => function ($method, $id = null) {
        switch ($method) {
            case "PUT":
                ShopRESTController::urediProfil($id);
                break;
            default: # GET
                ShopRESTController::profil($id);
                break;
        }
    },
            
    "/^api\/geslo\/(\d+)$/" => function ($method, $id = null) {
        switch ($method) {
            
            default: # PUT
                ShopRESTController::spremeniGeslo($id);
                break;
        }
    },
    
    "/^api\/zgodovinaNarocil\/(\d+)$/" => function ($method, $id = null) {
        switch ($method) {
            
            default: # GET
                ShopRESTController::vsaNarocilaStranke($id);
                break;
        }
    },
            
    "/^api\/podrobnostiNarocila\/(\d+)$/" => function ($method, $id = null) {
        switch ($method) {
            
            default: # GET
                ShopRESTController::podrobnostiNarocila($id);
                break;
        }
    },
            
    "/^api\/dodajIzdelek$/" => function ($method, $id = null) {
        switch ($method) {
            
            default: # POST
                ShopRESTController::dodajIzdelek($id);
                break;
        }
    },
            
    "/^api\/pridobiKosarico\/(\d+)$/" => function ($method, $id = null) {
        switch ($method) {
            
            default: # GET
                ShopRESTController::pridobiKosarico($id);
                break;
        }
    },
       
    "/^api\/izprazniKosarico\/(\d+)$/" => function ($method, $id = null) {
        switch ($method) {
            
            default: # DELETE
                ShopRESTController::izprazniKosarico($id);
                break;
        }
    },
            
    "/^api\/posodobiKosarico\/(\d+)$/" => function ($method, $id = null) {
        switch ($method) {
            
            default: # POST
                ShopRESTController::posodobiKosarico($id);
                break;
        }
    },
            
    "/^api\/potrdiNakup\/(\d+)$/" => function ($method, $id = null) {
        switch ($method) {
            
            default: # POST
                ShopRESTController::zakljuciNakup($id);
                break;
        }
    },
            
            
            
            
];

foreach ($urls as $pattern => $controller) {
    if (preg_match($pattern, $path, $params)) {
        try {
            $params[0] = $_SERVER["REQUEST_METHOD"];
            $controller(...$params);
        } catch (InvalidArgumentException $e) {
            ViewHelper::error404();
        } catch (Exception $e) {
            ViewHelper::displayError($e, true);
        }

        exit();
    }
}

ViewHelper::displayError(new InvalidArgumentException("No controller matched."), true);
