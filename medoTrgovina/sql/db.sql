-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema trgovinaTest
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema trgovinaTest
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `trgovinaTest` DEFAULT CHARACTER SET utf8 ;
USE `trgovinaTest` ;

-- -----------------------------------------------------
-- Table `trgovinaTest`.`artikel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `trgovinaTest`.`artikel` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `naziv` VARCHAR(45) NOT NULL,
  `opis` VARCHAR(255) NOT NULL,
  `cena` DOUBLE NOT NULL,
  `aktiviran` TINYINT(1) NOT NULL,
  `ocena` DOUBLE NOT NULL,
  `slika` VARCHAR(200) NOT NULL,
  `stOcen` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `trgovinaTest`.`stranka`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `trgovinaTest`.`stranka` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ime` VARCHAR(45) NOT NULL,
  `priimek` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `geslo` VARCHAR(255) NOT NULL,
  `naslov` VARCHAR(45) NOT NULL,
  `telSt` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `trgovinaTest`.`narocilo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `trgovinaTest`.`narocilo` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `status` VARCHAR(45) NOT NULL,
  `stranka_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_narocilo_stranka_idx` (`stranka_id` ASC),
  CONSTRAINT `fk_narocilo_stranka`
    FOREIGN KEY (`stranka_id`)
    REFERENCES `trgovinaTest`.`stranka` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `trgovinaTest`.`administrator`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `trgovinaTest`.`administrator` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ime` VARCHAR(45) NOT NULL,
  `priimek` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `geslo` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `trgovinaTest`.`prodajalec`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `trgovinaTest`.`prodajalec` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ime` VARCHAR(45) NOT NULL,
  `priimek` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `geslo` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `trgovinaTest`.`artikel_has_narocilo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `trgovinaTest`.`artikel_has_narocilo` (
  `artikel_id` INT NOT NULL,
  `narocilo_id` INT NOT NULL,
  `stArtiklov` INT NOT NULL,
  INDEX `fk_artikel_has_narocilo_narocilo1_idx` (`narocilo_id` ASC),
  INDEX `fk_artikel_has_narocilo_artikel1_idx` (`artikel_id` ASC),
  PRIMARY KEY (`artikel_id`, `narocilo_id`),
  CONSTRAINT `fk_artikel_has_narocilo_artikel1`
    FOREIGN KEY (`artikel_id`)
    REFERENCES `trgovinaTest`.`artikel` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_artikel_has_narocilo_narocilo1`
    FOREIGN KEY (`narocilo_id`)
    REFERENCES `trgovinaTest`.`narocilo` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;