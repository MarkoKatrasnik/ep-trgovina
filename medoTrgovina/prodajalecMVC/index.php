<?php

// enables sessions for the entire app
session_start();

require_once("controller/ProdajalecController.php");

define("BASE_URL", rtrim($_SERVER["SCRIPT_NAME"], "index.php"));
define("IMAGES_URL", rtrim($_SERVER["SCRIPT_NAME"], "index.php") . "static/images/");
define("CSS_URL", rtrim($_SERVER["SCRIPT_NAME"], "index.php") . "static/css/");

$path = isset($_SERVER["PATH_INFO"]) ? trim($_SERVER["PATH_INFO"], "/") : "";

$urls = [
    "/^$/" => function () {
        ViewHelper::redirect(BASE_URL . "prijavaProdajalec");
    },
            
            
    # PRODAJALEC
    "/^prijavaProdajalec$/" => function ($method) {
        
        ProdajalecController::prijavaProdajalecForm();
        
    },
            
    "/^prijavaProdajalecPotrdi/" => function ($method) {
        if ($method == "POST") {
            ProdajalecController::prijavaProdajalecAdd();
        }
    },
     
    "/^domovProdajalec$/" => function ($method) {
        ProdajalecController::domov();
    },        
     
    "/^profilProdajalec$/" => function ($method) {
        ProdajalecController::profilProdajalec();
    }, 
            
    "/^profilProdajalecUredi$/" => function ($method) {
        if ($method == "POST") {
            ProdajalecController::profilProdajalecUrediIzvedi();
        } else {
            ProdajalecController::profilProdajalecUrediForm();
        }
    }, 
    
    "/^spremeniGesloProdajalec$/" => function ($method) {
        if ($method == "POST") {
            ProdajalecController::spremeniGesloIzvedi();
        } else {
            ProdajalecController::spremeniGesloForm();
        }
    }, 
     
    "/^strankeProdajalec\/?(\d+)?$/" => function ($method, $id = null) {
        if ($id == null) {
            ProdajalecController::strankeList();
        } else {
            
            
            if ($method == "POST") {
                ProdajalecController::strankaUrediIzvedi($id);
                #echo 'Je POST';
            }
            else {
                ProdajalecController::strankaUrediForm($id);
                #echo 'Ni POST';
            }
        }
    },            
    
    "/^strankaAktivacijaDeaktivacijaPrikaz\/?(\d+)?$/" => function ($method, $id = null) {
        if ($id == null) {
            ProdajalecController::strankeList();
        } else {           
            if ($method == "POST") {
                #ProdajalecController::strankaUrediIzvedi($id);
                echo 'Je Post';
            }
            else {
                #ProdajalecController::strankaUrediForm($id);
                #echo 'Ni Post';
                ProdajalecController::strankaAktivacija($id);
            }
        }
    },  
            

    "/^strankaAktivacijaDeaktivacijaMenjava\/?(\d+)?$/" => function ($method, $id = null) {
        if ($id == null) {
            ProdajalecController::strankeList();
        } else {           
            if ($method == "POST") {
                #ProdajalecController::strankaUrediIzvedi($id);
                #echo 'Je Post';
                ProdajalecController::strankaAktivacijaDeaktivacija($id);
            }
            else {
                #ProdajalecController::strankaUrediForm($id);
                echo 'Ni Post';
                
            }
        }
    },
            
    
    "/^artikelAktivacijaDeaktivacijaPrikaz\/?(\d+)?$/" => function ($method, $id = null) {
        if ($id == null) {
            ProdajalecController::artikliList();
        } else {           
            if ($method == "POST") {
                #ProdajalecController::strankaUrediIzvedi($id);
                echo 'Je Post';
            }
            else {
                #ProdajalecController::strankaUrediForm($id);
                #echo 'Ni Post';
                ProdajalecController::artikelAktivacija($id);
            }
        }
    },  
            

    "/^artikelAktivacijaDeaktivacijaMenjava\/?(\d+)?$/" => function ($method, $id = null) {
        if ($id == null) {
            ProdajalecController::artikliList();
        } else {           
            if ($method == "POST") {
                #ProdajalecController::strankaUrediIzvedi($id);
                #echo 'Je Post';
                ProdajalecController::artikelAktivacijaDeaktivacija($id);
            }
            else {
                #ProdajalecController::strankaUrediForm($id);
                echo 'Ni Post';
                
            }
        }
    },                       
            
            
    "/^dodajStranko$/" => function ($method) {
        if ($method == "POST") {
            ProdajalecController::dodajStrankoAdd();
        } else {
            ProdajalecController::dodajStrankoForm();
        }
    },        
            
            
    "/^spremeniGesloStrankaProdajalec\/?(\d+)?$/" => function ($method, $id = null) {
        if ($method == "POST") {
            ProdajalecController::spremeniGesloStrankaIzvedi($id);
        } else {
            ProdajalecController::spremeniGesloStrankaForm($id);
        }
    },               
      
           
    "/^artikliProdajalec\/?(\d+)?$/" => function ($method, $id = null) {
        if ($id == null) {
            ProdajalecController::artikliList();
        } else {
            
            
            if ($method == "POST") {
                ProdajalecController::artikelUrediIzvedi($id);
            }
            else {
                ProdajalecController::artikelUrediForm($id);
            }
        }
    },             
            
            
    "/^dodajArtikel$/" => function ($method) {
        if ($method == "POST") {
            ProdajalecController::dodajArtikelAdd();
        } else {
            ProdajalecController::dodajArtikelForm();
        }
    },
            
    "/^urediArtikel\/?(\d+)?$/" => function ($method, $id = null) {
        if ($method == "POST") {
            ProdajalecController::artikelUrediIzvedi();
        } else {
            ProdajalecController::artikelUrediForm($id);
        }
    },
    
    "/^oddanaNarocila$/" => function ($method) {
        if ($method == "POST") {
            #ProdajalecController::dodajStrankoAdd();
            echo 'Je post';
            #ProdajalecController::oddanaNarocilaList();
        } else {
            #ProdajalecController::dodajStrankoForm();
            #echo 'Ni post';
            ProdajalecController::oddanaNarocilaList();
        }
    },
    
    "/^oddanaNarocilaPotrjeno\/?(\d+)?$/" => function ($method, $id = null) {
        if ($method == "POST") {
            #ProdajalecController::dodajStrankoAdd();
            #echo 'Je post';
            #ProdajalecController::oddanaNarocilaList();
            ProdajalecController::oddanaNarocilaPotrjenaList($id);
        } else {
            #ProdajalecController::dodajStrankoForm();
            echo 'Ni post';
            #ProdajalecController::oddanaNarocilaPotrjenaList();
        }
    },
            
    "/^oddanaNarocilaPreklicano\/?(\d+)?$/" => function ($method, $id = null) {
        if ($method == "POST") {
            #ProdajalecController::dodajStrankoAdd();
            #echo 'Je post';
            #ProdajalecController::oddanaNarocilaList();
            ProdajalecController::oddanaNarocilaPreklicanaList($id);
        } else {
            #ProdajalecController::dodajStrankoForm();
            echo 'Ni post';
            #ProdajalecController::oddanaNarocilaPotrjenaList();
        }
    },
            
            
            
    "/^zgodovinaNarocil$/" => function ($method) {
        if ($method == "POST") {
            #ProdajalecController::dodajStrankoAdd();
            echo 'Je post';
            #ProdajalecController::oddanaNarocilaList();
        } else {
            #ProdajalecController::dodajStrankoForm();
            #echo 'Ni post';
            ProdajalecController::potrjenaNarocilaList();
        }
    },
            
    "/^zgodovinaNarocilStorniraj\/?(\d+)?$/" => function ($method, $id = null) {
        if ($method == "POST") {
            #ProdajalecController::dodajStrankoAdd();
            #echo 'Je post';
            #ProdajalecController::oddanaNarocilaList();
            ProdajalecController::potrjenaNarocilaStornirajList($id);
        } else {
            #ProdajalecController::dodajStrankoForm();
            echo 'Ni post';
            #ProdajalecController::oddanaNarocilaPotrjenaList();
        }
    },
            
    "/^dnevnik$/" => function ($method) {
        ProdajalecController::dnevnik();
    },
            
    "/^uspesnaOdjava$/" => function ($method) {
        ProdajalecController::uspesnaOdjava();
    },
    
];

foreach ($urls as $pattern => $controller) {
    if (preg_match($pattern, $path, $params)) {
        try {
            $params[0] = $_SERVER["REQUEST_METHOD"];
            $controller(...$params);
        } catch (InvalidArgumentException $e) {
            ViewHelper::error404();
        } catch (Exception $e) {
            ViewHelper::displayError($e, true);
        }

        exit();
    }
}

ViewHelper::displayError(new InvalidArgumentException("No controller matched."), true);
