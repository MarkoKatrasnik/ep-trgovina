<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">

<style>
    body {
        margin:0;
    }

    ul.navigacija {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
        background-color: #4CAF50;
        position: fixed;
        top: 0;
        width: 100%;
    }

    li.navigacija {
        float: left;
    }

    a.navigacija{
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none; 
    }
    
    div.stran{
        padding:20px;
        margin-top:30px;
    }
  
     div.naslov {
         text-align: center;
     }
     
    ul.seznam {
        background: #818285;
        padding: 20px;
        list-style-type: none;
        width: 97%;
    }
    
    li.seznam {
        background: #f2f2f2;
        margin: 2px;
        padding: 20px;
    }
    
    a.seznam{
        color: #4CAF50;
        /*text-decoration:none;*/
    }
     
    table{
        margin-left: auto;
        margin-right: auto;
        width: 100%;
        text-align: left;
        table-layout: fixed;
    } 

    
</style>

<meta charset="UTF-8" />
<title>Seznam strank</title>


<body>
<?php
    if (isset($_SESSION["prodajalec"])):
?>

     <ul class="navigacija">
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/prodajalecMVC/domovProdajalec">Domov</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/prodajalecMVC/profilProdajalec">Profil</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/prodajalecMVC/uspesnaOdjava">Odjava</a></li>
   </ul>
    <div class="stran">
        <div class="naslov"><h1>Seznam strank</h1></div>

<ul class="seznam">

    <?php foreach ($items as $item): ?>
        <li class="seznam"><a class="seznam" href="<?= BASE_URL . "strankeProdajalec/" . $item["id"] ?>"><?= $item["ime"] ?> <?= $item["priimek"] ?></a> --> status: 
            
            <?php if (($item["aktiviran"])==1){ ?>
                <a class="seznam" href="<?= BASE_URL . "strankaAktivacijaDeaktivacijaPrikaz/" . $item["id"] ?>">aktiviran</a>
            <?php }else { ?>
                <a class="seznam" href="<?= BASE_URL . "strankaAktivacijaDeaktivacijaPrikaz/" . $item["id"] ?>">deaktiviran</a>  
            <?php } ?>
        </li>
    <?php endforeach; ?>

</ul>

<?php else: ?>
    <h1>Za uporabo te strani se je potrebno prijaviti</h1>   
    <p>[
    <a href="<?= BASE_URL . "prijavaProdajalec" ?>">Prijava prodajalca</a>
    ]</p>
<?php endif; ?>
    </div>
</body>