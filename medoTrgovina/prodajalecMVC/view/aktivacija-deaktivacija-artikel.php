<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">

<style>
    body {
        margin:0;
    }

    ul.navigacija {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
        background-color: #4CAF50;
        position: fixed;
        top: 0;
        width: 100%;
    }

    li.navigacija {
        float: left;
    }

    a.navigacija{
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none; 
    }
    
    div.stran{
        padding:20px;
        margin-top:30px;
    }
  
     div.naslov {
         text-align: center;
     }
     
   a.link{
        color: #4CAF50;
        /*text-decoration:none;*/
    }
     
    table{
        margin-left: auto;
        margin-right: auto;
        width: 100%;
        text-align: left;
        table-layout: fixed;
    } 

    button {
         
         background-color: #4CAF50;
         color: white;
         padding: 14px 20px;
         margin: 8px 0;
         border: none;
         border-radius: 4px;
         cursor: pointer;
     }

     button:hover {
         background-color: #45a049;
     }
     
     div.aktivacija {
         border-radius: 5px;
         background-color: #f2f2f2;
         padding: 20px;
         margin-left: 300px;
         margin-right: 300px;
         text-align: center;
     }
    
</style>

<meta charset="UTF-8" />
<title>Status artikla</title>

<body>
<?php
    if (isset($_SESSION["prodajalec"])):
?>

<ul class="navigacija">
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/prodajalecMVC/domovProdajalec">Domov</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/prodajalecMVC/profilProdajalec">Profil</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/prodajalecMVC/uspesnaOdjava">Odjava</a></li>
   </ul>
    <div class="stran">
        <div class="naslov"><h1>Status artikla</h1></div>

<p>Želite prikazati seznam vseh artiklov? Pojdite na <a class="link" href="<?= BASE_URL . "artikliProdajalec" ?>">seznam artiklov</a></p>

<div class="aktivacija">
    <form action="<?= BASE_URL . "artikelAktivacijaDeaktivacijaMenjava/" . $items["id"] ?>" method="post">
    <?php if (($items["aktiviran"])==1){ ?>


        <p>Artikel <?= $items["naziv"] ?> je trenutno aktiviran.</p>
        <p>Želite deaktivirati?</p>

        <p><button>Deaktiviraj</button></p>


    <?php }else { ?>
        <p>Artikel <?= $items["naziv"] ?> je trenutno deaktiviran.</p>
        <p>Želite aktivirati?</p>

        <p><button>Aktiviraj</button></p>
    <?php } ?>

    </form>
</div>

<?php else: ?>
    <h1>Za uporabo te strani se je potrebno prijaviti</h1>   
    <p>[
    <a href="<?= BASE_URL . "prijavaProdajalec" ?>">Prijava prodajalca</a>
    ]</p>
<?php endif; ?>
    </div>
</body>
