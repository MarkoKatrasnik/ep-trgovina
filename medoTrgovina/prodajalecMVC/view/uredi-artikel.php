<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">

<style>
        body {
            margin:0;
        }
        
        ul.navigacija {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #4CAF50;
            position: fixed;
            top: 0;
            width: 100%;
        }
        
        li.navigacija {
            float: left;
        }
        
        a.navigacija{
                display: block;
                color: white;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none; 
        }
        
        div.stran{
            padding:20px;
            margin-top:30px;
        }
        
        input[type=text], input[type=password], input[type=email], input[type=number], input[type=file], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        button {
            width: 100%;
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        button:hover {
            background-color: #45a049;
        }

        div.urejanje {
            border-radius: 5px;
            background-color: #f2f2f2;
            padding: 20px;
            margin-left: 300px;
            margin-right: 300px;
        }
        
        div.naslov {
            text-align: center;
        }

        a.link{
        color: #4CAF50;
        /*text-decoration:none;*/
        }
        
        img.slika{
            display: block;
            margin-left: auto;
            margin-right: auto;
        }
        
    </style> 

<meta charset="UTF-8" />
<title>Urejanje artikla</title>


<body>
<?php
    if (isset($_SESSION["prodajalec"])):
?>

    <ul class="navigacija">
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/prodajalecMVC/domovProdajalec">Domov</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/prodajalecMVC/profilProdajalec">Profil</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/prodajalecMVC/uspesnaOdjava">Odjava</a></li>
   </ul>
    <div class="stran">
        <div class="naslov"><h1>Urejanje artikla</h1></div>
        <p>Želite prikazati seznam vseh artiklov? Pojdite na <a class="link" href="<?= BASE_URL . "artikliProdajalec" ?>">seznam artiklov</a></p>
        <div class="urejanje">
            <form action="<?= BASE_URL . "urediArtikel" ?>" method="post" enctype="multipart/form-data">
                <p><label>Naziv: <input type="text" name="naziv" value="<?= $items["naziv"] ?>" autofocus /></label></p>
                <p><label>Opis: <input type="text" name="opis" value="<?= $items["opis"] ?>" /></label></p>
                <p><label>Cena: <input type="number" name="cena" value="<?= $items["cena"] ?>" step="0.01"/></label></p>
                <label>Trenutna slika:</label>
                <img class="slika" src="../../static/images/<?= $items["slika"] ?>" alt="<?= $items["slika"] ?>" width="200px">
                <p><label>Zamenjaj sliko: <input type="file" name="slika" accept="image/*" value="<?= $items["slika"] ?>"/></label><p>
                <input type="hidden" name="id" value="<?= $items["id"] ?>" />
                <input type="hidden" name="slk" value="<?= $items["slika"] ?>" />

                <p><button>Posodobi artikel</button></p>
            </form>
        </div>
 

    <?php else: ?>
        <h1>Za uporabo te strani se je potrebno prijaviti</h1>   
        <p>[
        <a href="<?= BASE_URL . "prijavaProdajalec" ?>">Prijava prodajalca</a>
        ]</p>
    <?php endif; ?>
</div>
</body>