<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="../static/css/style.css">

<style>
    body {
        margin:0;
    }

    ul.navigacija {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
        background-color: #4CAF50;
        position: fixed;
        top: 0;
        width: 100%;
    }

    li.navigacija {
        float: left;
    }

    a.navigacija{
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none; 
    }
    
    div.stran{
        padding:20px;
        margin-top:30px;
    }
  
     div.naslov {
         text-align: center;
     }
     
    ul.seznam {
        background: #818285;
        padding: 20px;
        list-style-type: none;
        width: 97%;
    }
    
    div.potrjeno {
        background: #a6a7a9;
        /*margin: 2px;*/
        border-style: solid;
        border-color: #7AEC7E;
    }
    div.stornirano {
        background: #a6a7a9;
        /*margin: 2px;*/
        border-style: solid;
        border-color: #f2f2f2;
    }
    
    li.podseznam {
        background: #f9f9f9;
        margin: 2px;
        padding: 20px;
    }
    
    a.seznam{
        color: #4CAF50;
        /*text-decoration:none;*/
    }
     
    button {
        width: 100%;
        background-color: #4CAF50;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        border: none;
        border-radius: 4px;
        cursor: pointer;
    }

    button:hover {
        background-color: #45a049;
    }

    
</style>

<meta charset="UTF-8" />
<title>Zgodovina naročil</title>

<body>
<?php
    if (isset($_SESSION["prodajalec"])){
?>

<ul class="navigacija">
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/prodajalecMVC/domovProdajalec">Domov</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/prodajalecMVC/profilProdajalec">Profil</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/prodajalecMVC/uspesnaOdjava">Odjava</a></li>
   </ul>
    <div class="stran">
        <div class="naslov"><h1>Zgodovina naročil</h1></div>

<?php if(count($items) != 0){ ?>
<ul class="seznam">
    <?php $previousValue = null?>

    <?php foreach ($items as $item): ?>
    
     
     <?php if($item["statusNarocila"] == 'Potrjeno'){ ?> 
 
        <?php $next = next($items); ?>
        
            <?php if($previousValue["datumNarocila"] != $item["datumNarocila"]){?>
                <div class="potrjeno">
                <li class="podseznam">Stranka <a class="seznam" href="<?= BASE_URL . "strankeProdajalec/" . $item["idStranke"] ?>"><?= $item["imeStranke"] ?> <?= $item["priimekStranke"] ?></a> 
                    je naročila:</li>
                 <li class="podseznam">"<a class="seznam" href="<?= BASE_URL . "artikelAktivacijaDeaktivacijaPrikaz/" . $item["idArtikla"] ?>"><?= $item["nazivArtikla"] ?></a>" v količini: <?= $item["ahnStArtiklov"] ?> </li>
            <?php }elseif ($item["datumNarocila"] == $previousValue["datumNarocila"]) {?>
               <li class="podseznam"> "<a class="seznam" href="<?= BASE_URL . "artikelAktivacijaDeaktivacijaPrikaz/" . $item["idArtikla"] ?>"><?= $item["nazivArtikla"] ?></a>" v količini: <?= $item["ahnStArtiklov"] ?> </li>  
                
            <?php }?>
                 
            <?php if($item["datumNarocila"] != $next["datumNarocila"]){?>
                <li class="podseznam">Datum: <?= $item["datumNarocila"] ?></li>
            <li class="podseznam"><form action="<?= BASE_URL . "zgodovinaNarocilStorniraj/" . $item["idNarocila"] ?>" method="post">
                <button>Storniraj naročilo</button>          
            </form></li>
               </div>
            <?php }?>
            <?php $previousValue = $item;?>

    <?php }else{?>
          
            <?php $next = next($items); ?>
        
            <?php if($previousValue["datumNarocila"] != $item["datumNarocila"]){?>
            <div class="stornirano">
                <li class="podseznam">Stornirano naročilo stranke <a class="seznam" href="<?= BASE_URL . "strankeProdajalec/" . $item["idStranke"] ?>"><?= $item["imeStranke"] ?> <?= $item["priimekStranke"] ?></a> 
                    je:</li>     
        
                <li class="podseznam">"<a class="seznam" href="<?= BASE_URL . "artikelAktivacijaDeaktivacijaPrikaz/" . $item["idArtikla"] ?>"><?= $item["nazivArtikla"] ?></a>" v količini: <?= $item["ahnStArtiklov"] ?> </li>
            <?php }elseif ($item["datumNarocila"] == $previousValue["datumNarocila"]) {?>
               <li class="podseznam"> "<a class="seznam" href="<?= BASE_URL . "artikelAktivacijaDeaktivacijaPrikaz/" . $item["idArtikla"] ?>"><?= $item["nazivArtikla"] ?></a>" v količini: <?= $item["ahnStArtiklov"] ?> </li>  
                
            <?php }?>
                 
            <?php if($item["datumNarocila"] != $next["datumNarocila"]){?>
                <li class="podseznam">Datum: <?= $item["datumNarocila"] ?></li>   
                </div>
            <?php }?>
                
            <?php $previousValue = $item;?>           
        <?php } ?>    
                
    <?php endforeach; ?>
    </ul>
    <?php }else{ ?>
        <p>Ni naročil!</p>
        <?php } ?>
    <?php }else{ ?>
    <h1>Za uporabo te strani se je potrebno prijaviti</h1>   
    <p>[
    <a href="<?= BASE_URL . "prijavaProdajalec" ?>">Prijava prodajalca</a>
    ]</p>
    <?php } ?>
    </div>
</body>