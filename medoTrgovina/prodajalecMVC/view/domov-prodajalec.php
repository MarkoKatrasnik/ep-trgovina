<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">

<style>
    body {
        margin:0;
    }

    ul.navigacija {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
        background-color: #4CAF50;
        position: fixed;
        top: 0;
        width: 100%;
    }

    li.navigacija {
        float: left;
    }

    a.navigacija{
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none; 
    }
    
    div.stran{
        padding:20px;
        margin-top:30px;
    }
  
     div.naslov {
         text-align: center;
     }
     
     ul.seznam {
        background: #818285;
        padding: 20px;
        list-style-type: none;
        width: 97%;
    }
    
    li.seznam {
        background: #f2f2f2;
        margin: 2px;
        padding: 20px;
    }
    
    a.seznam{
        color: #4CAF50;
        /*text-decoration:none;*/
    }
    
</style>

<meta charset="UTF-8" />
<title>Domov - prodajalec</title>


<body>
<?php
    if (isset($_SESSION["prodajalec"])):
        
        ?>

        <p>[
        <a href="<?= BASE_URL . "uspesnaOdjava" ?>">Odjava</a>
        ]</p>

            <ul class="navigacija">


         <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/prodajalecMVC/domovProdajalec">Domov</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/prodajalecMVC/profilProdajalec">Profil</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/prodajalecMVC/uspesnaOdjava">Odjava</a></li>
    </ul>
    <div class="stran">
        <div class="naslov"><h1>Prijavljeni ste kot prodajalec</h1></div>

        <ul class="seznam">
            <li class="seznam"><p><a class="seznam" href="<?= BASE_URL . "strankeProdajalec" ?>">Seznam strank</a></p></li>
            <li class="seznam"><p><a class="seznam" href="<?= BASE_URL . "dodajStranko" ?>">Dodaj stranko</a></p></li>
            <li class="seznam"><p><a class="seznam" href="<?= BASE_URL . "artikliProdajalec" ?>">Seznam artiklov</a></p></li>
            <li class="seznam"><p><a class="seznam" href="<?= BASE_URL . "dodajArtikel" ?>">Dodaj artikel</a></p></li>
            <li class="seznam"><p><a class="seznam" href="<?= BASE_URL . "oddanaNarocila" ?>">Seznam oddanih naročil</a></p></li>
            <li class="seznam"><p><a class="seznam" href="<?= BASE_URL . "zgodovinaNarocil" ?>">Zgodovina naročil</a></p></li>
            <li class="seznam"><p><a class="seznam" href="<?= BASE_URL . "dnevnik" ?>">Dnevnik</a></p></li>
        </ul>
        
    <?php else: ?>
        <h1>Za uporabo te strani se je potrebno prijaviti</h1>   
        <p>[
        <a href="<?= BASE_URL . "prijavaProdajalec" ?>">Prijava prodajalca</a>
        ]</p>
    <?php endif; ?>
    </div>
</body>