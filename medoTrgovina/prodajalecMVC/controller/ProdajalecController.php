<?php

require_once("../model/ProdajalecDB.php");
require_once("../model/ShopDB.php");
require_once("ViewHelper.php");

class ProdajalecController {
   public static function prijavaProdajalecAdd() {
        $data = filter_input_array(INPUT_POST, self::getRulesPrijava());
        var_dump($data);
        if (self::checkValues($data)) {
            
            try {
                //pridobi podatke o prodajalcu
                $uporabnik = ProdajalecDB::getProdajalec(["email" => $data["email"]]);

                if (password_verify($data["geslo"], $uporabnik["geslo"])) {
                    //session_start();
                    $_SESSION["prodajalec"] = $uporabnik["id"];
                    
                    $dataDnevnik["prodajalec_id"] = $_SESSION["prodajalec"];
                    $dataDnevnik["aktivnost"] = "Prijava";
                    $dataDnevnik["podrobnosti"] = "/";
                    $idDnevnik = ProdajalecDB::insertDnevnik($dataDnevnik);
                    
                    echo ViewHelper::redirect(BASE_URL . "domovProdajalec");
                }
                else {
                    $data["sporocilo"] = 'Geslo ni pravilno!';
                    self::prijavaProdajalecForm($data);
                }
            } catch (Exception $e) {
                die($e->getMessage());
                #self::prijavaProdajalecForm($data);
            }
        }
        else {
            self::prijavaProdajalecForm($data);
        }
    }
 
    
    
    public static function prijavaProdajalecForm($values = [
        "email" => "",
        "geslo" => "",
        "sporocilo" => ""
    ]) {
        session_regenerate_id();
        unset($_SESSION["stranka"]);
        unset($_SESSION["admin"]);
        unset($_SESSION["prodajalec"]);
        
        $cert_mesto = $_SERVER['SSL_CLIENT_S_DN_L'];
        $cert_podjetje = $_SERVER['SSL_CLIENT_S_DN_O'];
        $cert_delovnoMesto = $_SERVER['SSL_CLIENT_S_DN_OU'];
        $cert_ime = $_SERVER['SSL_CLIENT_S_DN_CN'];
        $cert_email = $_SERVER['SSL_CLIENT_S_DN_Email'];
        
                
           //     filter_input(INPUT_SERVER, "SSL_CLIENT_CERT");
        
        
        if (!$cert_mesto || !$cert_podjetje || !$cert_delovnoMesto || !$cert_ime || !$cert_email ) {
            die('err: Spremenljivka SSL_CLIENT_CERT ni nastavljena.');
        }
        
        $values["email"] = $cert_email;
        
        
        if ($cert_podjetje == 'Trgovina' && $cert_delovnoMesto == 'Prodajalec') {
            try {
                //pridobi podatke o prodajalcu
                $uporabnik = ProdajalecDB::getProdajalec(["email" => $values["email"]]);


            } catch (Exception $e) {
                #die($e->getMessage());
                $url = substr(BASE_URL, 0, -14);
                #var_dump($url);
                echo ViewHelper::redirect("$url/items");
            }
            #var_dump($uporabnik);
            $values["aktiviran"] = $uporabnik["aktiviran"];
            echo ViewHelper::render("view/prijava-prodajalec.php", $values);
        } else {
            #echo("Podatki: $podjetje, $delovnoMesto, $commonname");

            $url = substr(BASE_URL, 0, -14);
            echo ViewHelper::redirect("$url/items"); # preusmeri na stran za izbiro med prijavo kot stranka ali prodajalec ali administrator, po možnosti opozori,
                                                    # po možnosti opozori, da certifikat ni bil pravilen
        }
        
    }
    
    public static function domov() {
        echo ViewHelper::render("view/domov-prodajalec.php");
    }
    
    public static function profilProdajalec() {
        if (isset($_SESSION["prodajalec"])) {
            $id = $_SESSION["prodajalec"];
            echo ViewHelper::render("view/profil-prodajalec.php", [
                "items" => ProdajalecDB::getProfil(["id" => $id])
            ]);
        }
        else {
            echo ViewHelper::render("view/profil-prodajalec.php");
        }
    }
    
    public static function profilProdajalecUrediForm() {
        $id = $_SESSION["prodajalec"];
        echo ViewHelper::render("view/profil-uredi-prodajalec.php", [
            "items" => ProdajalecDB::getProfil(["id" => $id])
        ]);
    }
    
    public static function profilProdajalecUrediIzvedi() {
        
        $data = filter_input_array(INPUT_POST, self::getRulesUrediProfil());
        $data["id"] = $_SESSION["prodajalec"];

        if (self::checkValues($data)) {
            //$idPrijavljen = $_SESSION["stranka"];
            
            $id = ProdajalecDB::updateProfil($data);
            
            $dataDnevnik["prodajalec_id"] = $_SESSION["prodajalec"];
            $dataDnevnik["aktivnost"] = "Urejanje lastnega profila";
            $dataDnevnik["podrobnosti"] = "/";
            $idDnevnik = ProdajalecDB::insertDnevnik($dataDnevnik);
            
            echo ViewHelper::redirect(BASE_URL . "profilProdajalec");
        } else {
            self::profilProdajalecUrediForm($data);
        }
    }
    
    public static function spremeniGesloForm($values = [
        "geslo" => ""
    ]) {
        echo ViewHelper::render("view/spremeni-geslo-prodajalec.php", $values);
    }
    
    public static function spremeniGesloIzvedi() {
        
        $data = filter_input_array(INPUT_POST, self::getRulesSpremeniGeslo());
        $data["id"] = $_SESSION["prodajalec"];

        if (self::checkValues($data)) {
            $data["geslo"] = password_hash($data["geslo"], PASSWORD_DEFAULT);
            $id = ProdajalecDB::updateGeslo($data);
            
            $dataDnevnik["prodajalec_id"] = $_SESSION["prodajalec"];
            $dataDnevnik["aktivnost"] = "Sprememba lastnega gesla";
            $dataDnevnik["podrobnosti"] = "/";
            $idDnevnik = ProdajalecDB::insertDnevnik($dataDnevnik);
            
            echo ViewHelper::redirect(BASE_URL . "profilProdajalec");
        } else {
            self::spremeniGesloForm($data);
        }
    }
    
    public static function strankeList() {
        echo ViewHelper::render("view/seznam-stranke.php", [
            "items" => ProdajalecDB::getStrankeList()
        ]);
    }
    
    public static function dodajStrankoForm($values = [
        "ime" => "",
        "priimek" => "",
        "email" => "",
        "geslo" => "",
        "telSt" => "",     
        "naslov" => "",
        "postnaSt" => "1000"
    ]) {
        #session_regenerate_id();
        echo ViewHelper::render("view/dodaj-stranko.php", $values);
    }
    
    public static function dodajStrankoAdd() {                    
        $data = filter_input_array(INPUT_POST, self::getRulesDodajStranko());
      
        #var_dump($data);
        
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $data2 = array('secret' => '6Lfa9j4UAAAAAHMlTIQACm_eZ4UnZJTSaNoCvtnL', 'response' => $data['g-recaptcha-response']);

        // use key 'http' even if you send the request to https://...
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data2)
            )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        if ($result === FALSE) { var_dump($data2); }

        #var_dump($result);
        $captcha = json_decode($result);
        $captchaRes = get_object_vars($captcha)['success'];
        
        if (self::checkValues($data) && $captchaRes==true) {
            $data["geslo"] = password_hash($data["geslo"], PASSWORD_DEFAULT);
            $vnos["ime"] = $data["ime"];
            $vnos["priimek"] = $data["priimek"];
            $vnos["email"] = $data["email"];
            $vnos["geslo"] = $data["geslo"];
            $vnos["telSt"] = $data["telSt"];
            $vnos["naslov"] = $data["naslov"];
            $vnos["postnaSt"] = $data["posta"];
            $vnos["aktiviran"] = 1;
            
            $dataDnevnik["prodajalec_id"] = $_SESSION["prodajalec"];
            $dataDnevnik["aktivnost"] = "Dodan profil stranke";
            $dataDnevnik["podrobnosti"] = "Dodana stranka: " . $vnos["ime"] . " " . $vnos["priimek"];
            $idDnevnik = ProdajalecDB::insertDnevnik($dataDnevnik);
            
            $id = ProdajalecDB::dodajStranko($vnos);
            echo ViewHelper::redirect(BASE_URL . "dodajStranko");      
        } else {
            $data["postnaSt"] = $data["posta"];
            self::dodajStrankoForm($data);
        }
    }   
    
    
    
    public static function strankaUrediForm($id) {
        echo ViewHelper::render("view/stranka-uredi.php", [
            "items" => ProdajalecDB::getStranka(["id" => $id])
        ]);
    }
    
    public static function strankaUrediIzvedi($id) {
        
        $data = filter_input_array(INPUT_POST, self::getRulesUrediProfilStranka());
        $data["id"] = $id;

        if (self::checkValues($data)) {
            //$idPrijavljen = $_SESSION["stranka"];
            $data["postnaSt"] = $data["posta"];
            $id = ProdajalecDB::updateStrankaProfil($data);
            
            $dataDnevnik["prodajalec_id"] = $_SESSION["prodajalec"];
            $dataDnevnik["aktivnost"] = "Posodobitev profila stranke";
            $dataDnevnik["podrobnosti"] = "Stranka: " . $data["ime"] . " " . $data["priimek"];
            $idDnevnik = ProdajalecDB::insertDnevnik($dataDnevnik);
            
            echo ViewHelper::redirect(BASE_URL . "strankeProdajalec");
        } else {
            self::profilProdajalecUrediForm($data);
        }
        
    }
    
    public static function spremeniGesloStrankaForm($id, $values = [
        "geslo" => ""
    ]) {
        
        $values["id"] = $id;
        #var_dump($values);
        echo ViewHelper::render("view/spremeni-geslo-stranka-prodajalec.php", ["items" => ['id' => $id]]);
    }
    
    public static function spremeniGesloStrankaIzvedi($id) {
        
        $data = filter_input_array(INPUT_POST, self::getRulesSpremeniGeslo());
        $data["id"] = $id;

        if (self::checkValues($data)) {
            $data["geslo"] = password_hash($data["geslo"], PASSWORD_DEFAULT);
            $id = ProdajalecDB::updateStrankaGeslo($data);
            
            $dataDnevnik["prodajalec_id"] = $_SESSION["prodajalec"];
            $dataDnevnik["aktivnost"] = "Spremeba gesla stranke";
            $dataDnevnik["podrobnosti"] = "Id stranke: " . $data["id"];
            $idDnevnik = ProdajalecDB::insertDnevnik($dataDnevnik);
            
            echo ViewHelper::redirect(BASE_URL . "strankeProdajalec");
        } else {
            self::spremeniGesloStrankaForm($data);
        }
    }
    
    public static function artikliList() {
        echo ViewHelper::render("view/seznam-artikel.php", [
            "items" => ProdajalecDB::getArtikliList()
        ]);
    }
    
    
    public static function dodajArtikelForm($values = [
        "naziv" => "",
        "opis" => "",
        "cena" => "",
        "slika" => ""
    ]) {
        
        echo ViewHelper::render("view/dodaj-artikel.php", $values);
    }
    
    public static function dodajArtikelAdd() {
        
        $data = filter_input_array(INPUT_POST, self::getRulesDodajArtikel());
        
        if (2>1) {
            
            $imgFile = $_FILES["slika"]["name"];
            $tmp_dir = $_FILES["slika"]['tmp_name'];
            $imgSize = $_FILES["slika"]['size'];
            
            $upload_dir = '../static/images/'; // upload directory
 
            $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension

            // valid image extensions
            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions

            // rename uploading image
            $userpic = rand(1000,1000000).".".$imgExt;
            
            // allow valid image file formats
            if(in_array($imgExt, $valid_extensions)){   
             // Check file size '5MB'
             if($imgSize < 5000000)    {
                 #echo 'Premik file-a!!!';
              move_uploaded_file($tmp_dir,$upload_dir.$userpic);
             }
             else{
              $errMSG = "Sorry, your file is too large.";
             }
            }
            else{
             $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";  
            }
            $data["slika2"] = $userpic;
            #var_dump($data);
            if(!isset($errMSG)) {
                $id = ProdajalecDB::dodajArtikel($data);
                
                $dataDnevnik["prodajalec_id"] = $_SESSION["prodajalec"];
                $dataDnevnik["aktivnost"] = "Dodan nov artikel";
                $dataDnevnik["podrobnosti"] = "Artikel: " . $data["naziv"];
                $idDnevnik = ProdajalecDB::insertDnevnik($dataDnevnik);
                
                echo ViewHelper::redirect(BASE_URL . "dodajArtikel"); 
            }else{
                echo 'Napaka pri dodajanju artikla! (278)';
            }
            
            
        } else {
            self::dodajArtikelForm($data);
        }
    }
    
    public static function artikelUrediForm($id) {
        
        echo ViewHelper::render("view/uredi-artikel.php", [
            "items" => ShopDB::get(["id" => $id])
        ]);
    }
    
    public static function artikelUrediIzvedi() {
        $data = filter_input_array(INPUT_POST, self::getRulesUrediArtikel());
        
        $imgFile = $_FILES["slika"]["name"];
        $tmp_dir = $_FILES["slika"]['tmp_name'];
        $imgSize = $_FILES["slika"]['size'];
        
        if($imgFile){
         $upload_dir = '../static/images/'; // upload directory
         $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
         $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
         $userpic = rand(1000,1000000).".".$imgExt;
         if(in_array($imgExt, $valid_extensions))
         {   
          if($imgSize < 5000000)
          {
           unlink($upload_dir.$data['slk']);
           move_uploaded_file($tmp_dir,$upload_dir.$userpic);
           $data["slika"] = "185241.jpg";
           $data["slika2"] = $userpic;
          }
          else
          {
           $errMSG = "Sorry, your file is too large it should be less then 5MB";
          }
         }
         else
         {
          $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";  
         } 
        }
        else
        {
         // if no image selected the old image remain as it is.
         $data["slika2"] = $data["slk"]; // old image from database
         $data["slika"] = "185241.jpg";
        }
        #var_dump($data);
        
        if (self::checkValues($data)) {
            //$idPrijavljen = $_SESSION["stranka"];
            
            $id = $data["id"];
            $id2 = ProdajalecDB::updateArtikel($data);
            
            $dataDnevnik["prodajalec_id"] = $_SESSION["prodajalec"];
            $dataDnevnik["aktivnost"] = "Posodobitev artikla";
            $dataDnevnik["podrobnosti"] = "Artikel: " . $data["naziv"];
            $idDnevnik = ProdajalecDB::insertDnevnik($dataDnevnik);
            
            echo ViewHelper::redirect(BASE_URL . "urediArtikel/$id"); //treba zamenjati
        } else {
            self::artikelUrediForm($data);
        }
    }

        public static function checkValues($input) {
        if (empty($input)) {
            return FALSE;
        }

        $result = TRUE;
        foreach ($input as $value) {
            $result = $result && $value != false;
        }

        return $result;
    }
    
    public static function dnevnik() {
        if (isset($_SESSION["prodajalec"])) {
            $id = $_SESSION["prodajalec"];
            echo ViewHelper::render("view/dnevnik-prodajalec.php", [
                "items" => ProdajalecDB::getDnevnik(["prodajalec_id" => $id])
            ]);
        }
        else {
            echo ViewHelper::render("view/dnevnik-admin.php");
        }
    }
    
    public static function uspesnaOdjava() {
        unset($_SESSION["prodajalec"]);
        echo ViewHelper::render("view/uspesnaOdjava-prodajalec.php");
    }
    
    
        public static function getRulesDodajStranko() {
        return [
            'ime' => FILTER_SANITIZE_SPECIAL_CHARS,
            'priimek' => FILTER_SANITIZE_SPECIAL_CHARS,
            'email' => FILTER_SANITIZE_SPECIAL_CHARS,
            'geslo' => FILTER_SANITIZE_SPECIAL_CHARS,
            'telSt' => FILTER_SANITIZE_SPECIAL_CHARS,
            'naslov' => FILTER_SANITIZE_SPECIAL_CHARS,
            'posta' => FILTER_SANITIZE_SPECIAL_CHARS,
            'g-recaptcha-response' => FILTER_SANITIZE_SPECIAL_CHARS
        ];
    }
    
    public static function getRulesDodajArtikel() {
        return [
            'naziv' => FILTER_SANITIZE_SPECIAL_CHARS,
            'opis' => FILTER_SANITIZE_SPECIAL_CHARS,
            'cena' => FILTER_SANITIZE_SPECIAL_CHARS,
            'aktiviran' => FILTER_SANITIZE_SPECIAL_CHARS,
            'ocena' => FILTER_SANITIZE_SPECIAL_CHARS,
            'slika' => FILTER_SANITIZE_SPECIAL_CHARS,
            'stOcen' => FILTER_SANITIZE_SPECIAL_CHARS
        ];
    }
    
    public static function getRulesUrediArtikel() {
        return [
            'naziv' => FILTER_SANITIZE_SPECIAL_CHARS,
            'opis' => FILTER_SANITIZE_SPECIAL_CHARS,
            'cena' => FILTER_SANITIZE_SPECIAL_CHARS,
            'id' => FILTER_SANITIZE_SPECIAL_CHARS,
            'slika' => FILTER_SANITIZE_SPECIAL_CHARS,
            'slk' => FILTER_SANITIZE_SPECIAL_CHARS
        ];
    }
    
    public static function getRulesPrijava() {
        return [
            'email' => FILTER_SANITIZE_SPECIAL_CHARS,
            'geslo' => FILTER_SANITIZE_SPECIAL_CHARS
        ];
    }
    
    public static function getRulesUrediProfil() {
        return [
            'ime' => FILTER_SANITIZE_SPECIAL_CHARS,
            'priimek' => FILTER_SANITIZE_SPECIAL_CHARS,
            'email' => FILTER_SANITIZE_SPECIAL_CHARS
        ];
    }
    
    public static function getRulesSpremeniGeslo() {
        return [
            'geslo' => FILTER_SANITIZE_SPECIAL_CHARS
        ];
    }

    public static function getRulesSpremeniAktivacijo() {
        return [
            'aktiviran' => FILTER_SANITIZE_SPECIAL_CHARS
        ];
    }

    
    public static function strankaAktivacija($id) {
        echo ViewHelper::render("view/aktivacija-deaktivacija-stranke.php", [
            "items" => ProdajalecDB::getAktivacija(["id" => $id])
        ]);
    }
    
    public static function strankaAktivacijaDeaktivacija($id) {
        #echo ViewHelper::render("view/aktivacija-deaktivacija-stranke.php", [
        #    "items" => ProdajalecDB::updateAktivacija(["id" => $id])
        #]);
        
        $data = filter_input_array(INPUT_POST, self::getRulesSpremeniAktivacijo());
        $data["id"] = $id;

        if (self::checkValues($data)) {
            $id = ProdajalecDB::updateAktivacija($data);
            
            $dataDnevnik["prodajalec_id"] = $_SESSION["prodajalec"];
            $dataDnevnik["aktivnost"] = "Aktivacija/deaktivacija stranke";
            $dataDnevnik["podrobnosti"] = "Id stranke: " . $data["id"];
            $idDnevnik = ProdajalecDB::insertDnevnik($dataDnevnik);
            
            echo ViewHelper::redirect(BASE_URL . "strankaAktivacijaDeaktivacijaPrikaz");
        } else {
            #self::spremeniGesloStrankaForm($data);
            echo 'Napaka pri statusu stranke!!!';
        }
    }
    
    public static function artikelAktivacija($id) {
        echo ViewHelper::render("view/aktivacija-deaktivacija-artikel.php", [
            "items" => ProdajalecDB::getAktivacijaArtikla(["id" => $id])
        ]);
    }
    
    public static function artikelAktivacijaDeaktivacija($id) {
       
        $data = filter_input_array(INPUT_POST, self::getRulesSpremeniAktivacijo());
        $data["id"] = $id;

        if (self::checkValues($data)) {
            $id = ProdajalecDB::updateAktivacijaArtikla($data);
            
            $dataDnevnik["prodajalec_id"] = $_SESSION["prodajalec"];
            $dataDnevnik["aktivnost"] = "Aktivacija/deaktivacija artikla";
            $dataDnevnik["podrobnosti"] = "Id artikla: " . $data["id"];
            $idDnevnik = ProdajalecDB::insertDnevnik($dataDnevnik);
            
            echo ViewHelper::redirect(BASE_URL . "artikelAktivacijaDeaktivacijaPrikaz");
        } else {
            #self::spremeniGesloStrankaForm($data);
            echo 'Napaka pri statusu artikla!!!';
        }
    }
    
    public static function oddanaNarocilaList() {
        echo ViewHelper::render("view/seznam-oddanih-narocil.php", [
            "items" => ProdajalecDB::getOddanaNarocila()
        ]);
    }
    
    public static function oddanaNarocilaPotrjenaList($id) {
        $data = filter_input_array(INPUT_POST, self::getRulesSpremeniNarocilo());
        $data["id"] = $id;

        if (self::checkValues($data)) {
            $id = ProdajalecDB::updateNarociloPotrjeno($data);
            
            $dataDnevnik["prodajalec_id"] = $_SESSION["prodajalec"];
            $dataDnevnik["aktivnost"] = "Potrjeno naročilo";
            $dataDnevnik["podrobnosti"] = "Id naročila: " . $data["id"];
            $idDnevnik = ProdajalecDB::insertDnevnik($dataDnevnik);
            
            echo ViewHelper::redirect(BASE_URL . "oddanaNarocila");
        } else {
            #self::spremeniGesloStrankaForm($data);
            echo 'Napaka pri potrditvi narocila!!!';
        }
    }
    
    public static function oddanaNarocilaPreklicanaList($id) {
        $data = filter_input_array(INPUT_POST, self::getRulesSpremeniNarocilo());
        $data["id"] = $id;

        if (self::checkValues($data)) {
            $id = ProdajalecDB::updateNarociloPreklicano($data);
            
            $dataDnevnik["prodajalec_id"] = $_SESSION["prodajalec"];
            $dataDnevnik["aktivnost"] = "Preklicano naročilo";
            $dataDnevnik["podrobnosti"] = "Id naročila: " . $data["id"];
            $idDnevnik = ProdajalecDB::insertDnevnik($dataDnevnik);
            
            echo ViewHelper::redirect(BASE_URL . "oddanaNarocila");
        } else {
            #self::spremeniGesloStrankaForm($data);
            echo 'Napaka pri preklicu narocila!!!';
        }
    }
    
    public static function getRulesSpremeniNarocilo() {
        return [
            'status' => FILTER_SANITIZE_SPECIAL_CHARS
        ];
    }
    
    public static function potrjenaNarocilaList() {
        echo ViewHelper::render("view/zgodovina-narocil.php", [
            "items" => ProdajalecDB::getPotrjenaNarocila()
        ]);
    }
    
    public static function potrjenaNarocilaStornirajList($id) {
        $data = filter_input_array(INPUT_POST, self::getRulesSpremeniNarocilo());
        $data["id"] = $id;

        if (self::checkValues($data)) {
            $id = ProdajalecDB::updateNarociloStornirano($data);
            
            $dataDnevnik["prodajalec_id"] = $_SESSION["prodajalec"];
            $dataDnevnik["aktivnost"] = "Stornirano naročilo";
            $dataDnevnik["podrobnosti"] = "Id naročila: " . $data["id"];
            $idDnevnik = ProdajalecDB::insertDnevnik($dataDnevnik);
            
            echo ViewHelper::redirect(BASE_URL . "zgodovinaNarocil");
        } else {
            #self::spremeniGesloStrankaForm($data);
            echo 'Napaka pri storniranju narocila!!!';
        }
    }
    
    public static function getRulesUrediProfilStranka() {
        return [
            'ime' => FILTER_SANITIZE_SPECIAL_CHARS,
            'priimek' => FILTER_SANITIZE_SPECIAL_CHARS,
            'email' => FILTER_SANITIZE_SPECIAL_CHARS,
            'telSt' => FILTER_SANITIZE_SPECIAL_CHARS,
            'naslov' => FILTER_SANITIZE_SPECIAL_CHARS,
            'posta' => FILTER_SANITIZE_SPECIAL_CHARS
        ];
    }
    
    
}
