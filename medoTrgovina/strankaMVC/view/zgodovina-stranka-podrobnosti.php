

<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">

<style>
    body {
        margin:0;
    }

    ul.navigacija {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
        background-color: #4CAF50;
        position: fixed;
        top: 0;
        width: 100%;
    }

    li.navigacija {
        float: left;
    }

    a.navigacija{
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none; 
    }
    
    div.stran{
        padding:20px;
        margin-top:30px;
    }
  
     div.naslov {
         text-align: center;
     }
     
     ul.seznam {
        background: #818285;
        padding: 20px;
        list-style-type: none;
    }
    
    li.seznam {
        background: #f2f2f2;
        margin: 2px;
        padding: 20px;
    }
    
    a.seznam{
        color: #4CAF50;
        /*text-decoration:none;*/
    }
    
</style>

<meta charset="UTF-8" />
<title>Podrobnosti naročila</title>

<body>
<?php
    if (isset($_SESSION["stranka"])):
        
        if($items[0]["stranka_id"] == $_SESSION["stranka"]):
        
            ?>
    
    
        <ul class="navigacija">
         <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/strankaMVC/artikli">Domov</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/strankaMVC/profil">Profil</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/strankaMVC/prijava">Odjava</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/strankaMVC/zgodovinaStranka">Zgodovina</a></li>
        </ul>
        <div class="stran">
        <div class="naslov"><h1>Podrobnosti naročila</h1></div>


            <ul class="seznam">
            <?php
                $znesek = 0;

                foreach ($items as $item):

                    $znesek += $item["cena"] * $item["stArtiklov"];
                    ?>
                    <li class="seznam">
                        <p> <?= $item["stArtiklov"] ?> &times; <?= $item["naziv"] ?> (<?= $item["cena"] ?> EUR) </p>
                    </li>
                <?php endforeach; ?>

                </ul>
                <ul class="seznam">
                    <li class="seznam"><p>Skupaj: <b><?= number_format($znesek, 2) ?> EUR</b></p></li>
                <li class="seznam"><p>Čas oddaje: <b><?= $item["datum"] ?></b></p></li>
                <li class="seznam"><p>Status naročila: <b><?= $item["status"] ?> </b></p></li>
                </ul>
        <?php else: ?>
            <h1>Do svojih naročil dostopajte s strani <a href="<?= BASE_URL . "zgodovinaStranka" ?>">Zgodovina naročil</a></h1> 
        <?php endif; ?>
    <?php else: ?>
        <h1>Za uporabo te strani se je potrebno prijaviti</h1>   
        <p>[
        <a href="<?= BASE_URL . "prijava" ?>">Prijava</a>
        ]</p>
    <?php endif; ?>

</body>