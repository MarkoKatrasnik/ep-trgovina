

<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="../static/css/style.css">

<style>
    body {
        margin:0;
    }

    ul.navigacija {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
        background-color: #4CAF50;
        position: fixed;
        top: 0;
        width: 100%;
    }

    li.navigacija {
        float: left;
    }

    a.navigacija{
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none; 
    }
    
    div.stran{
        padding:20px;
        margin-top:30px;
    }
  
     div.naslov {
         text-align: center;
     }
     
     ul.seznam {
        background: #818285;
        padding: 20px;
        list-style-type: none;
    }
    
    li.seznam {
        background: #f2f2f2;
        margin: 2px;
        padding: 20px;
    }
    
    a.seznam{
        color: #4CAF50;
        /*text-decoration:none;*/
    }
    
    input[type=submit] {
        width: 100%;
        background-color: #4CAF50;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        border: none;
        border-radius: 4px;
        cursor: pointer;
    }

    input[type=submit]:hover {
        background-color: #45a049;
    }
</style>

<meta charset="UTF-8" />
<title>Blagajna</title>
<body>
<?php
    if (isset($_SESSION["stranka"])):
        
        ?>


    <ul class="navigacija">
         <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/strankaMVC/artikli">Domov</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/strankaMVC/profil">Profil</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/strankaMVC/prijava">Odjava</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/strankaMVC/zgodovinaStranka">Zgodovina</a></li>
    </ul>
    <div class="stran">
        <div class="naslov"><h1>Blagajna</h1></div>


        
      <ul class="seznam">

        <?php
              
            require_once("../model/ShopDB.php");

            if ($items):
                $znesek = 0;

                foreach ($items as $izdelek):
                    $item = ShopDB::get(["id" => $izdelek["artikel_id"]]);
                    $znesek += $item["cena"] * $izdelek["stArtiklov"];
                    ?>
                    <li class="seznam">
                        <p> <?= $izdelek["stArtiklov"] ?> &times; <?= $item["naziv"] ?> (<?= $item["cena"] ?> EUR) </p>
                    </li>
                    
                <?php endforeach; ?>
                    
                </ul>

                <ul class="seznam">
                    <li class="seznam">
                <p>Skupaj: <b><?= number_format($znesek, 2) ?> EUR</b></p>
                    </li>
                    
                <form action="<?= BASE_URL . "nakupStranka" ?>" method="POST">
                    
                    <input type="submit" value="Potrdi nakup" />
                    <?php 
                        foreach($items as $izdelek)
                        {   
                            $st = $izdelek["stArtiklov"];
                            $id = $izdelek["artikel_id"];
                            echo '<input type="hidden" name="stArtiklov[]" value="'. $st. '">';
                            echo '<input type="hidden" name="artikel_id[]" value="'. $id. '">';
                                                    
                        }
                    ?>
                </form>
                    
                </ul>
            <?php else: ?>
                Košarica je prazna.                
            <?php endif; ?>
                
        
    <?php else: ?>
        <h1>Za uporabo te strani se je potrebno prijaviti</h1>   
        <p>[
        <a href="<?= BASE_URL . "prijava" ?>">Prijava</a>
        ]</p>
    <?php endif; ?>
    </div>
        </body>