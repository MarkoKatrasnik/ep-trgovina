

<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">

<style>
    body {
        margin:0;
    }

    ul.navigacija {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
        background-color: #4CAF50;
        position: fixed;
        top: 0;
        width: 100%;
    }

    li.navigacija {
        float: left;
    }

    a.navigacija{
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none; 
    }
    
    div.stran{
        padding:20px;
        margin-top:30px;
    }
  
     div.naslov {
         text-align: center;
     }
     
     ul.seznam {
        background: #818285;
        padding: 20px;
        list-style-type: none;
    }
    
    li.seznam {
        background: #f2f2f2;
        margin: 2px;
        padding: 20px;
    }
    
    a.seznam{
        color: #4CAF50;
        /*text-decoration:none;*/
    }
    
</style>

<meta charset="UTF-8" />
<title>Zgodovina naročil</title>

<body>
<?php
    if (isset($_SESSION["stranka"])):
        
        ?>
            <ul class="navigacija">
         <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/strankaMVC/artikli">Domov</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/strankaMVC/profil">Profil</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/strankaMVC/prijava">Odjava</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/strankaMVC/zgodovinaStranka">Zgodovina</a></li>
    </ul>
    <div class="stran">
        <div class="naslov"><h1>Zgodovina naročil</h1></div>

        <?php if(count($items) != 0){ ?>
        <ul class="seznam">

            <?php foreach ($items as $item): ?>
            <li class="seznam">
                <p><a class="seznam" href="<?= BASE_URL . "zgodovinaStranka/" . $item["id"] ?>"><?= $item["datum"] ?> [<?= $item["status"] ?>] </a></p></li>

            <?php endforeach; ?>

        </ul>
              <?php }else{ ?>         
                <p>Nič niste naročil!</p>
            <?php } ?>
    <?php else: ?>
        <h1>Za uporabo te strani se je potrebno prijaviti</h1>   
        <p>[
        <a href="<?= BASE_URL . "prijava" ?>">Prijava</a>
        ]</p>
    <?php endif; ?>

</body>