

<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="../../static/css/style.css">

<style>
    body {
        margin:0;
    }

    ul.navigacija {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
        background-color: #4CAF50;
        position: fixed;
        top: 0;
        width: 100%;
    }

    li.navigacija {
        float: left;
    }

    a.navigacija{
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none; 
    }
    
    div.stran{
        padding:20px;
        margin-top:30px;
    }
  
     div.naslov {
         text-align: center;
     }
     
    table.seznam{
        margin-right: auto;
        margin-left: 100px;
        background-color: #818285;
        padding:10px;
    } 
    
    table.kosarica{
        margin-left: auto;
        margin-right: auto;
        text-align: center;
        table-layout: fixed;
    }

    th.artikel, td.artikel {
        /*padding: 5px;*/
        text-align: left;  
        padding: 10px;
        background: #f2f2f2;
    }
    button {
        background-color: #4CAF50;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        border: none;
        border-radius: 4px;
        cursor: pointer;
    }

    button:hover {
        background-color: #45a049;
    }
       
    a.link{
    color: #4CAF50;
    /*text-decoration:none;*/
    }
    
    input.gumb {
         
         background-color: #4CAF50;
         color: white;
         padding: 14px 20px;
         margin: 8px 0;
         border: none;
         border-radius: 4px;
         cursor: pointer;
     }
</style>

<meta charset="UTF-8" />
<title>Podrobnosti</title>

<body>
    <ul class="navigacija">
         <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/strankaMVC/artikli">Domov</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/strankaMVC/profil">Profil</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/strankaMVC/prijava">Odjava</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/strankaMVC/zgodovinaStranka">Zgodovina</a></li>
    </ul>
    <div class="stran">
        <div class="naslov"><h1><?= $items["naziv"] ?></h1></div>


    <?php
        $id = $items["id"];
            ?>
        <!--<p>Ko si zadovoljen z nakupovanjem, pojdi na 
        <a class="link" href="<?= BASE_URL . "nakupStranka" ?>"> blagajno</a>.
        </p>-->
        <table class="seznam">
          <tr class="artikel">
            <td class="artikel" rowspan="4"><img src="../../static/images/<?= $items["slika"] ?>" alt="<?= $items["slika"] ?>" width="300px"></td>
            <td class="artikel">Cena: <b><?= $items["cena"] ?> EUR</b></td>
          </tr>
          <tr class="artikel">
            <td class="artikel">Ocena: <?= 
           $items["ocena"] == NULL ? "Artikel še ni ocenjen" : round($items["ocena"], 1) ?></i></td>
          </tr>
          <tr class="artikel">
            <td class="artikel">Opis: <i><?= $items["opis"] ?></i></td>
          </tr>
          <tr class="artikel">
            <td class="artikel"><form action="<?= BASE_URL . "artikli/$id" ?>" method="post">
                <input type="hidden" name="do" value="add_into_cart" />
                <input type="hidden" name="id" value="<?= $id ?>" />
                <div class="naslov"><button type="submit">V košarico</button></div>
            </form></td>
          </tr>         
            <?php
              if ($items["ocenjevanje"] == 1):

                  ?>
          <tr class="artikel">
          <form action="<?= BASE_URL . "artikli/$id" ?>" method="post">
          <td class="artikel"><input type="hidden" name="id" value="<?= $id ?>" />
              <input type="hidden" name="do" value="posodobi_oceno" />
              <p><label>Vaša ocena: <input type="number" name="strankaOcena" value="1" step="1" min="1" max="5"/></label></p></td>
          <td  class="artikel"><div class="naslov"><button type="submit">Oddaj oceno</button></div></td>
          </form>
          </tr>  
          <?php endif; ?>

        </table>
        
    

    <div id="cart">
        <div class="naslov"><h3>Košarica</h3></div>

        <?php

        require_once("../model/ShopDB.php");

        if ($items["kosarica"]):
            $znesek = 0;

            foreach ($items["kosarica"] as $indeks=>$kosarica):
                //$knjiga = BazaKnjig::vrniKnjigo($id);
                //$znesek += $knjiga->cena * $kolicina;
                $item = ShopDB::get(["id" => $kosarica["artikel_id"]]);
                //var_dump($knjiga);
                $znesek += $item["cena"] * $kosarica["stArtiklov"];
                $id_artikla = $kosarica["artikel_id"];
                ?>
                <form action="<?= BASE_URL . "artikli/$id" ?>" method="post">
                    <input type="hidden" name="do" value="update_cart" />
                    <input type="hidden" name="id" value="<?= $item["id"] ?>" />
                    <input type="number" name="kolicina" value="<?= $kosarica["stArtiklov"] ?>"
                           class="short_input" min="0"/>
                    &times; <?=
                    (strlen($item["naziv"]) < 30) ?
                            $item["naziv"] :
                            substr($item["naziv"], 0, 26) . " ..."
                    ?> 
                    <label><?=sprintf("&nbsp&nbsp(%.2f EUR)&nbsp&nbsp", $item["cena"]) ?></label>
                    <button type="submit">Posodobi</button> 
                </form>
            <?php endforeach; ?>

            <p>Total: <b><?= number_format($znesek, 2) ?> EUR</b></p>

            <table class="kosarica">
            <tr>
                <td><form action="<?= BASE_URL . "artikli/$id" ?>" method="POST">
                    <input type="hidden" name="do" value="purge_cart" />
                    <input class="gumb" type="submit" value="Izprazni košarico" />
                </form></td>
                <td><form action="<?= BASE_URL . "artikli/$id" ?>" method="GET">
                    <input type="hidden" name="do" value="refresh" />
                    <input class="gumb" type="submit" value="Osveži košarico" />
                </form></td>
            </tr>
            </table>
            <p>Pojdi na <a class="link" href="<?= BASE_URL . "nakupStranka" ?>">blagajno</a></p>
        <?php else: ?>
            Košarica je prazna.                
        <?php endif; ?>
    </div>




    </div>
</body>

