

<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="../static/css/style.css">
<style>
    body {
        margin:0;
    }

    ul.navigacija {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
        background-color: #4CAF50;
        position: fixed;
        top: 0;
        width: 100%;
    }

    li.navigacija {
        float: left;
    }

    a.navigacija{
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none; 
    }
    
    div.stran{
        padding:20px;
        margin-top:30px;
    }
  
     div.naslov {
         text-align: center;
     }
     
    ul.seznam {
        background: #818285;
        padding: 20px;
        list-style-type: none;
        width: 88%;
    }
    
    li.seznam {
        background: #f2f2f2;
        margin: 2px;
        padding: 20px;
    }
    
    a.seznam{
        color: #4CAF50;
        /*text-decoration:none;*/
    }
     
    table.seznam{
        margin-left: auto;
        margin-right: auto;
        width: 100%;
        text-align: left;
        table-layout: fixed;
    } 
    table.kosarica{
        margin-left: auto;
        margin-right: auto;
        text-align: center;
        table-layout: fixed;
    } 
    
    a.seznam{
        color: #4CAF50;
        /*text-decoration:none;*/
    }

    button {         
         background-color: #4CAF50;
         color: white;
         padding: 14px 20px;
         margin: 8px 0;
         border: none;
         border-radius: 4px;
         cursor: pointer;
     }

     button:hover {
         background-color: #45a049;
     }
     
    input.gumb {
         
         background-color: #4CAF50;
         color: white;
         padding: 14px 20px;
         margin: 8px 0;
         border: none;
         border-radius: 4px;
         cursor: pointer;
     }
    
</style>


<meta charset="UTF-8" />
<title>Vsi izdelki</title>

<?php
    if (isset($_SESSION["stranka"])):
        
        ?>
<body>
    
    <ul class="navigacija">
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/strankaMVC/artikli">Domov</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/strankaMVC/profil">Profil</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/strankaMVC/prijava">Odjava</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/strankaMVC/zgodovinaStranka">Zgodovina</a></li>
    </ul>
    <div class="stran">
    <div class="naslov"><h1>Vsi izdelki</h1></div>


<ul class="seznam">

    <?php 
        if ($items[0]):
            foreach($items[0] as $item):
             ?>
            
            <li class="seznam">
            <div class="artikel">
                
                <form action="<?= BASE_URL . "artikli" ?>" method="post">
                    
                    
                    <table class="seznam">
                        <tr>  
                            <input type="hidden" name="do" value="add_into_cart" />
                            <input type="hidden" name="id" value="<?= $item["id"] ?>" />
                            <td><p><a class="seznam" href="<?= BASE_URL . "artikli/" . $item["id"] ?>"><?= $item["naziv"] ?></a></p></td>
                            <td><p>Cena: <?= $item["cena"] ?> EUR</p></td>
                            <td><button type="submit">V košarico</button></td>
                        </tr>
                    </table>
                </form>
                   
            </div> </li>
    <?php endforeach; 
        else: ?>
    <p>V trgovini ni artiklov.  </p>              
    <?php endif; ?>

</ul>

<div id="cart">
    <div class="naslov"><h3>Košarica</h3></div>

    <?php
    
    require_once("../model/ShopDB.php");
    if ($items[1]):
        $znesek = 0;

        foreach ($items[1] as $indeks => $vrednost):
            $item = ShopDB::get(["id" => $vrednost["artikel_id"]]);
            $znesek += $item["cena"] * $vrednost["stArtiklov"];
            
            ?>
            <form action="<?= BASE_URL . "artikli" ?>" method="post">
                <input type="hidden" name="do" value="update_cart" />
                <input type="hidden" name="id" value="<?= $item["id"] ?>" />
                <input type="number" name="kolicina" value="<?= $vrednost["stArtiklov"] ?>"
                       class="short_input" min="0"/>
                &times; <?=
                (strlen($item["naziv"]) < 30) ?
                        $item["naziv"] :
                        substr($item["naziv"], 0, 26) . " ..."?>
                <label><?=sprintf("&nbsp&nbsp(%.2f EUR)&nbsp&nbsp", $item["cena"]) ?></label>
                <button type="submit">Posodobi</button> 
            </form>
        <?php endforeach; ?>

        <p>Total: <b><?= number_format($znesek, 2) ?> EUR</b></p>
        <table class="kosarica">
            <tr>
                <td><form action="<?= BASE_URL . "artikli" ?>" method="POST">
                    <input type="hidden" name="do" value="purge_cart" />
                    <input class="gumb" type="submit" value="Izprazni košarico" />
                    </form></td>

                <td><form action="<?= BASE_URL . "artikli" ?>" method="GET">
                    <input type="hidden" name="do" value="refresh" />
                    <input class="gumb" type="submit" value="Osveži košarico" />
                </form></td>
            </tr>
        </table>
        <p>Pojdi na <a class="seznam" href="<?= BASE_URL . "nakupStranka" ?>">blagajno</a></p>
    <?php else: ?>
        Košarica je prazna.                
    <?php endif; ?>
</div>

<?php else: ?>
        <h1>Za uporabo te strani se je potrebno prijaviti</h1>   
        <p>[
        <a href="<?= BASE_URL . "prijava" ?>">Prijava</a>
        ]</p>
    <?php endif; ?>

    </div>
</body>