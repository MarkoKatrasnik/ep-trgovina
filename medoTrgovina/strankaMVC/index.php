<?php

// enables sessions for the entire app
session_start();

require_once("controller/StrankaController.php");

define("BASE_URL", rtrim($_SERVER["SCRIPT_NAME"], "index.php"));
define("IMAGES_URL", rtrim($_SERVER["SCRIPT_NAME"], "index.php") . "static/images/");
define("CSS_URL", rtrim($_SERVER["SCRIPT_NAME"], "index.php") . "static/css/");

$path = isset($_SERVER["PATH_INFO"]) ? trim($_SERVER["PATH_INFO"], "/") : "";

$urls = [
    "/^$/" => function () {
        ViewHelper::redirect(BASE_URL . "prijava");
    },
    
    "/^registracija$/" => function ($method) {
        if ($method == "POST") {
            StrankaController::add();
        } else {
            StrankaController::addForm();
        }
    },
    "/^prijava$/" => function ($method) {
        if ($method == "POST") {
            StrankaController::prijavaAdd();
            #echo 'Vzpelo';
        } else {
            StrankaController::prijavaForm();
            #echo 'Ni vzpelo';
        }
    },
    "/^artikli\/?(\d+)?$/" => function ($method, $id = null) {
        
        if ($id == null) {
            if ($method == "POST") {
                StrankaController::dodajKosarica();
            }
            else {
                StrankaController::seznam();
            }
            
        } else {
            if ($method == "POST") {
                StrankaController::dodajKosaricaPodrobnosti($id);
            }
            else {
                StrankaController::podrobnosti($id);
            }
        }
        
        
    },
    
    "/^profil$/" => function ($method) {
        StrankaController::profil();
    },
    "/^profilUredi$/" => function ($method) {
        if ($method == "POST") {
            StrankaController::profilUrediIzvedi();
        } else {
            StrankaController::profilUrediForm();
        }
    },
    "/^spremeniGesloStranka$/" => function ($method) {
        if ($method == "POST") {
            StrankaController::spremeniGesloIzvedi();
        } else {
            StrankaController::spremeniGesloForm();
        }
    },
    "/^nakupStranka$/" => function ($method) {
        if ($method == "POST") {
            StrankaController::nakupIzvedi();
        } else {
            StrankaController::nakupForm();
        }
    },
    "/^zgodovinaStranka\/?(\d+)?$/" => function ($method, $id = null) {
        if ($id == null) {
            StrankaController::zgodovinaStranka();
        } else {
            StrankaController::zgodovinaStrankaPodrobnosti($id);
        }
    },
            
    "/^kodaAktivacije\/([a-zA-Z0-9&]+)/" => function ($method, $niz = null) {
        if ($niz == null) {
            #StrankaController::zgodovinaStranka();
            echo 'Napaka pri aktivaciji!';
        } else {
            StrankaController::aktivacijaNoveStranke($niz);
        }
    },
    
];

foreach ($urls as $pattern => $controller) {
    if (preg_match($pattern, $path, $params)) {
        try {
            $params[0] = $_SERVER["REQUEST_METHOD"];
            $controller(...$params);
        } catch (InvalidArgumentException $e) {
            ViewHelper::error404();
        } catch (Exception $e) {
            ViewHelper::displayError($e, true);
        }

        exit();
    }
}

ViewHelper::displayError(new InvalidArgumentException("No controller matched."), true);