<?php

require_once("../model/StrankaDB.php");
require_once("../model/ShopDB.php");
require_once("ViewHelper.php");
require '../PHPMailer/PHPMailerAutoload.php';

//session_start();

class StrankaController {

    public static function addForm($values = [
        "ime" => "",
        "priimek" => "",
        "email" => "",
        "geslo" => "",
        "telSt" => "",     
        "naslov" => "",
        "postnaSt" => "1000"
    ]) {
        session_regenerate_id();
        echo ViewHelper::render("view/registracija.php", $values);
    }

    
    public static function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
        return $randomString;
    }
    
    public static function add() {
        
        $data = filter_input_array(INPUT_POST, self::getRules());
        $naslov = BASE_URL;
        #var_dump($naslov);
        
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $data2 = array('secret' => '6Lfa9j4UAAAAAHMlTIQACm_eZ4UnZJTSaNoCvtnL', 'response' => $data['g-recaptcha-response']);

        // use key 'http' even if you send the request to https://...
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data2)
            )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        if ($result === FALSE) { var_dump($data2); }

        //var_dump($result);
        $captcha = json_decode($result);
        $captchaRes = get_object_vars($captcha)['success'];
        
        if (self::checkValues($data) && $captchaRes==true) {
            #var_dump($result);
            $data["geslo"] = password_hash($data["geslo"], PASSWORD_DEFAULT);
            $vnos["ime"] = $data["ime"];
            $vnos["priimek"] = $data["priimek"];
            $vnos["email"] = $data["email"];
            $vnos["geslo"] = $data["geslo"];
            $vnos["telSt"] = $data["telSt"];
            $vnos["naslov"] = $data["naslov"];
            $vnos["postnaSt"] = $data["posta"];
            $vnos["kodaAktivacije"] = self::generateRandomString(10);
            
            $id = StrankaDB::insert($vnos);
            
            #var_dump($id);
            //!!!!!!!!!!!!!pošiljanje maila!!!!!!!!!!!
            
            $mail = new PHPMailer();
            $mail->isSMTP();
            $mail->SMTPDebug=0;
            $mail->Debugoutput = 'html';
            $mail->Host = 'smtp.gmail.com'; 
            
            $mail->SMTPAuth = true; 
            $mail->Username = 'ep.trgovina.2017.2018@gmail.com';
            $mail->Password = 'ep20172018';  
            
            $mail->SMTPSecure = 'tls'; 
            $mail->Port = 587;
            
            $mail->SMTPOptions = array(
                'ssl'=> array(
                    "verify_peer"=>false,"verify_peer_name"=>false,"allow_self_signed"=>true));
  
            $mail->isHTML(true);  
            
            $mail->Subject = 'Registracija nove stranke';
            $mail->Body    = 'Registrirali ste se na spletno stran MedoTrgovina.'
                    . 'Prijavo potrdite s klikom na spletno stran:  <a href="https://localhost' . $naslov . 'kodaAktivacije/' .  $vnos["kodaAktivacije"] . '&' . $id . '">aktivacija spletne strani</a>';
            
            $mail->setFrom('ep.trgovina.2017.2018@gmail.com', 'MedoTrgovina');
            
            $mail->addAddress($data["email"]);
            
            if(!$mail->send()) {
                echo 'Message could not be sent.';
                echo 'Mailer Error: ' . $mail->ErrorInfo;
            } else {
                #echo 'Message has been sent';
                echo 'Potrdite vašo registracijo na mailu.';
            }
            
            
            #echo ViewHelper::redirect(BASE_URL . "prijava");
        } else {
            $data["postnaSt"] = $data["posta"];
            self::addForm($data);
        }
    }
    
    public static function aktivacijaNoveStranke($niz) {
        #var_dump($niz);
        $niz2 = explode('&', $niz);
        #var_dump($niz2);
        $koAct = $niz2[0];
        $idRegistriranega = $niz2[1];
        $registriranec = StrankaDB::getKodaAktivacije(["id" => $idRegistriranega]);
        
        if($registriranec["kodaAktivacije"] == $koAct){
            echo ViewHelper::render("view/uspesna-registracija.php", $registriranec);
            $aktiviran = StrankaDB::postaneAktiviran($registriranec);
        }else{
            echo 'Napaka pri potrditvi registracije.';
        }
        
        #echo ViewHelper::render("view/podrobnosti-stranka.php", $items);
    }
    
    
    public static function prijavaForm($values = [
        "email" => "",
        "geslo" => "",
        "sporocilo" => ""
    ]) {
        session_regenerate_id();
        unset($_SESSION["stranka"]);
        unset($_SESSION["admin"]);
        unset($_SESSION["prodajalec"]);
        unset($_SESSION["kos"]);
        echo ViewHelper::render("view/prijava.php", $values);
    }
    
    public static function prijavaAdd() {
        
        $data = filter_input_array(INPUT_POST, self::getRulesPrijava());
        #var_dump($data);
        if (self::checkValues($data)) {
            
            try {
                //pridobi podatke o uporabniku
                $uporabnik = StrankaDB::getUser(["email" => $data["email"]]);

                if (password_verify($data["geslo"], $uporabnik["geslo"])) {
                    //session_start();
                    if($uporabnik["aktiviran"] == 1) {
                        $_SESSION["stranka"] = $uporabnik["id"];
                        #var_dump($_SESSION);
                    echo ViewHelper::redirect(BASE_URL . "artikli");
                    }
                    else {
                        $data["sporocilo"] = 'Uporabnik ni aktiviran!';
                        self::prijavaForm($data);
                    }
                    
                    
                }
                else {
                    $data["sporocilo"] = 'Geslo ni pravilno!';
                    self::prijavaForm($data);
                }
            } catch (Exception $e) {
                die($e->getMessage());
                self::prijavaForm($data);
            }
        }
        else {
            self::prijavaForm($data);
        }
    }
    
    public static function podrobnosti($id) {
        
        if (isset($_SESSION["stranka"])){
            $items = ShopDB::get(["id" => $id]);
            $id_stranke["stranka_id"] = $_SESSION["stranka"];
            $kosarica = StrankaDB::getKosarica($id_stranke);
            $stOcen = StrankaDB::stOcen(["idStranke" => $_SESSION["stranka"], "id" => $id]);
            #var_dump($stOcen);
            if($stOcen["stevilo"] > 0) {
                $items["ocenjevanje"] = 0;
            }
            else {
                $items["ocenjevanje"] = 1;
            }
            $items["kosarica"] = $kosarica;
            echo ViewHelper::render("view/podrobnosti-stranka.php", ["items" => $items]);
        }
        else {
            ViewHelper::redirect(BASE_URL . "prijava");
        }
    }
    
    public static function posodobiOceno() {
        
        $data = INPUT_POST;

        if (self::checkValues($data)) {  
            $id = StrankaDB::updateOcena($data);
            $id2 = data["id"];
            var_dump($data);
            #echo ViewHelper::redirect(BASE_URL . "podrobnosti/$id2");
        } else {
            self::podrobnosti($data);
        }
    }

    public static function seznam() {
        if (isset($_SESSION["stranka"])){
            $id_stranke["stranka_id"] = $_SESSION["stranka"];
            $kosarica = StrankaDB::getKosarica($id_stranke);
            $items[0] = ShopDB::getAll();
            $items[1] = $kosarica;

           echo ViewHelper::render("view/seznam-stranka.php", ["items" => $items]);
        }else {
            ViewHelper::redirect(BASE_URL . "prijava");
        }
    }
    
    public static function profil() {
        if (isset($_SESSION["stranka"])) {
            $id = $_SESSION["stranka"];
            echo ViewHelper::render("view/profil.php", [
                "items" => StrankaDB::getProfil(["id" => $id])
            ]);
        }
        else {
            echo ViewHelper::render("view/profil.php");
        }
    }
    
    public static function profilUrediForm() {
        if (isset($_SESSION["stranka"])) {
            $id = $_SESSION["stranka"];
            echo ViewHelper::render("view/profil-uredi.php", [
                "items" => StrankaDB::getProfil(["id" => $id])
            ]);
        } else {
            echo ViewHelper::render("view/profil-uredi.php");
        }
        
    }
    
    public static function profilUrediIzvedi() {
        
        $data = filter_input_array(INPUT_POST, self::getRulesUrediProfil());
        $data["id"] = $_SESSION["stranka"];

        if (self::checkValues($data)) {
            $data["postnaSt"] = $data["posta"];
            
            $id = StrankaDB::updateProfil($data);
            
            echo ViewHelper::redirect(BASE_URL . "profil");
        } else {
            self::addForm($data);
        }
    }
    
    public static function spremeniGesloForm($values = [
        "geslo" => ""
    ]) {
        echo ViewHelper::render("view/spremeni-geslo-stranka.php", $values);
    }
    
    public static function spremeniGesloIzvedi() {
        
        $data = filter_input_array(INPUT_POST, self::getRulesSpremeniGeslo());
        $data["id"] = $_SESSION["stranka"];

        if (self::checkValues($data)) {
            //$idPrijavljen = $_SESSION["stranka"];
            $data["geslo"] = password_hash($data["geslo"], PASSWORD_DEFAULT);
            $id = StrankaDB::updateGeslo($data);
            echo ViewHelper::redirect(BASE_URL . "profil");
        } else {
            self::spremeniGesloForm($data);
        }
    }
    
    public static function dodajKosarica() {
        $validationRules = ['do' => [
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => [
                    "regexp" => "/^(add_into_cart|update_cart|purge_cart)$/"
                ]
            ],
            'id' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0]
            ],
            'kolicina' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0]
            ]
        ];
        $data = filter_input_array(INPUT_POST, $validationRules);
        
        //var_dump($data);
        $data["stranka_id"] = $_SESSION["stranka"];
        $data["artikel_id"] = $data["id"];
        
        switch ($data["do"]) {
            case "add_into_cart":
                try {
                    $vsebujeArtikel = StrankaDB::preveriKosarico($data);
                    if ($vsebujeArtikel) {
                        $data["stArtiklov"] = $vsebujeArtikel[0]["stArtiklov"]+1;
                        StrankaDB::posodobiKosarico($data);
                    }
                    else {
                        StrankaDB::dodajVKosarico($data);
                    }
                    
                } catch (Exception $exc) {
                    die($exc->getMessage());
                }
                break;
            case "update_cart":
                try {
                    $vsebujeArtikel = StrankaDB::preveriKosarico($data);
                    if ($vsebujeArtikel) {
                        if ($data["kolicina"] > 0) {
                            $data["stArtiklov"] = $data["kolicina"];
                            StrankaDB::posodobiKosarico($data);
                        } else {
                            StrankaDB::odstraniIzKosarice($data);
                        }
                    }   
                } catch (Exception $exc) {
                    die($exc->getMessage());
                }
                
                break;
            case "purge_cart":
                StrankaDB::izprazniKosarico($data);
                break;
            default:
                break;
        }
        
        self::seznam();
    }
    
    public static function dodajKosaricaPodrobnosti($id) {
        $validationRules = ['do' => [
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => [
                    "regexp" => "/^(add_into_cart|update_cart|purge_cart|posodobi_oceno)$/"
                ]
            ],
            'id' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0]
            ],
            'kolicina' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0]
            ],
            'strankaOcena' => FILTER_VALIDATE_INT
        ];
        $data = filter_input_array(INPUT_POST, $validationRules);
        
        //var_dump($data);
        $data["stranka_id"] = $_SESSION["stranka"];
        $data["artikel_id"] = $data["id"];
        switch ($data["do"]) {
            case "add_into_cart":
                try {
                    $vsebujeArtikel = StrankaDB::preveriKosarico($data);
                    if ($vsebujeArtikel) {
                        $data["stArtiklov"] = $vsebujeArtikel[0]["stArtiklov"]+1;
                        StrankaDB::posodobiKosarico($data);
                    }
                    else {
                        StrankaDB::dodajVKosarico($data);
                    }
                    
                } catch (Exception $exc) {
                    die($exc->getMessage());
                }
                break;
            case "update_cart":
                try {
                    $vsebujeArtikel = StrankaDB::preveriKosarico($data);
                    if ($vsebujeArtikel) {
                        if ($data["kolicina"] > 0) {
                            $data["stArtiklov"] = $data["kolicina"];
                            StrankaDB::posodobiKosarico($data);
                        } else {
                            StrankaDB::odstraniIzKosarice($data);
                        }
                    }   
                } catch (Exception $exc) {
                    die($exc->getMessage());
                }
                
                break;
            case "purge_cart":
                StrankaDB::izprazniKosarico($data);
                break;
            case "posodobi_oceno":
                #$data2 = StrankaDB::getArtikelOcena($data);
                $data2["idStranke"] = $_SESSION["stranka"];
                $data2["id"] = $data["id"];
                $data2["ocena"] = $data["strankaOcena"];
                #var_dump($data2);
                StrankaDB::dodajOceno($data2);
                break;
            default:
                break;
        }
        
        self::podrobnosti($id);
    }
    
    public static function nakupForm() {
        if(isset($_SESSION["stranka"])) {
            $data["stranka_id"] = $_SESSION["stranka"];
            $vsebujeArtikel = StrankaDB::getKosarica($data);
            if ($vsebujeArtikel) {
                echo ViewHelper::render("view/nakup-stranka.php", ["items" => $vsebujeArtikel]);
            }
            else {
                echo ViewHelper::render("view/nakup-stranka.php", ["items" => null]);
            }
        }
        else {
            echo ViewHelper::render("view/nakup-stranka.php", ["items" => null]);
        }
        
    }
    
    public static function nakupIzvedi() {
        $data["status"] = "Oddano";
        $data["stranka_id"] = $_SESSION["stranka"];
        $data1 = $_POST["stArtiklov"];
        $data2 = $_POST["artikel_id"];
        
        //$data2 = $data1["kosarica"];
        
        //var_dump($data1);
        //var_dump($data2);
        
        
        $idNarocilo = StrankaDB::insertNarocilo($data);

        #var_dump($vsebujeArtikel);
        if ($data1) {
            for($i = 0; $i < count($data1); $i++) {
                #foreach ($vsebujeArtikel as $artikel_id => $steviloArtiklov) {
                #var_dump($vsebujeArtikel[$i]);
                $data2["narocilo_id"] = $idNarocilo;
                $data2["artikel_id"] = $data2[$i];
                $data2["stArtiklov"] = $data1[$i];

                $idPovezava = StrankaDB::insertPovezava($data2);
        
            }
            
        }  
        StrankaDB::izprazniKosarico($data);
        echo ViewHelper::redirect(BASE_URL . "artikli");
        
    }
    
    public static function zgodovinaStranka() {
        if (isset($_SESSION["stranka"])) {
            $stranka_id = $_SESSION["stranka"];
            echo ViewHelper::render("view/zgodovina-stranka.php", [
                "items" => StrankaDB::getNarocilo(["stranka_id" => $stranka_id])
            ]);
        }
        else {
            echo ViewHelper::render("view/zgodovina-stranka.php");
        }
    }
    
    public static function zgodovinaStrankaPodrobnosti($id) {
        echo ViewHelper::render("view/zgodovina-stranka-podrobnosti.php", [
                "items" => StrankaDB::getNarociloPodrobnosti(["id" => $id])
        ]);
    }


    /**
     * Returns TRUE if given $input array contains no FALSE values
     * @param type $input
     * @return type
     */
    public static function checkValues($input) {
        if (empty($input)) {
            return FALSE;
        }

        $result = TRUE;
        foreach ($input as $value) {
            $result = $result && $value != false;
        }

        return $result;
    }

    /**
     * Returns an array of filtering rules for manipulation books
     * @return type
     */
    public static function getRules() {
        return [
            'ime' => FILTER_SANITIZE_SPECIAL_CHARS,
            'priimek' => FILTER_SANITIZE_SPECIAL_CHARS,
            'email' => FILTER_SANITIZE_SPECIAL_CHARS,
            'geslo' => FILTER_SANITIZE_SPECIAL_CHARS,
            'telSt' => FILTER_SANITIZE_SPECIAL_CHARS,
            'naslov' => FILTER_SANITIZE_SPECIAL_CHARS,
            'posta' => FILTER_SANITIZE_SPECIAL_CHARS,
            'g-recaptcha-response' => FILTER_SANITIZE_SPECIAL_CHARS
        ];
    }
    
    public static function getRulesPrijava() {
        return [
            'email' => FILTER_SANITIZE_SPECIAL_CHARS,
            'geslo' => FILTER_SANITIZE_SPECIAL_CHARS
        ];
    }
    
    public static function getRulesUrediProfil() {
        return [
            'ime' => FILTER_SANITIZE_SPECIAL_CHARS,
            'priimek' => FILTER_SANITIZE_SPECIAL_CHARS,
            'email' => FILTER_SANITIZE_SPECIAL_CHARS,
            'telSt' => FILTER_SANITIZE_SPECIAL_CHARS,
            'naslov' => FILTER_SANITIZE_SPECIAL_CHARS,
            'posta' => FILTER_SANITIZE_SPECIAL_CHARS
        ];
    }
    
    public static function getRulesSpremeniGeslo() {
        return [
            'geslo' => FILTER_SANITIZE_SPECIAL_CHARS
        ];
    }
    


}
