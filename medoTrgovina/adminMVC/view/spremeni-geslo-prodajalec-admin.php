

<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">

<style>
    body {
        margin:0;
    }

    ul.navigacija {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
        background-color: #4CAF50;
        position: fixed;
        top: 0;
        width: 100%;
    }

    li.navigacija {
        float: left;
    }

    a.navigacija{
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none; 
    }
    
    div.stran{
        padding:20px;
        margin-top:30px;
    }
  
     div.naslov {
         text-align: center;
     }
     
        button {
           width: 100%;
           background-color: #4CAF50;
           color: white;
           padding: 14px 20px;
           margin: 8px 0;
           border: none;
           border-radius: 4px;
           cursor: pointer;
       }

       button:hover {
           background-color: #45a049;
       }

       div.urejanje {
           border-radius: 5px;
           background-color: #f2f2f2;
           padding: 20px;
           margin-left: 300px;
           margin-right: 300px;
       }

        input[type=password]{
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }
       
</style> 

<meta charset="UTF-8" />
<title>Sprememba gesla</title>

<body>
<?php
    if (isset($_SESSION["admin"])):
?>

  <ul class="navigacija">
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/adminMVC/domovAdmin">Domov</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/adminMVC/profilAdmin">Profil</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/adminMVC/uspesnaOdjava">Odjava</a></li>
    </ul>
    <div class="stran">
        <div class="naslov"><h1>Sprememba gesla prodajalca - admin</h1></div>
    <div class="urejanje">
    <form action="<?= BASE_URL . "spremeniGesloProdajalecAdmin/" . $items["id"] ?>" method="post">
        <p><label>Vpiši novo geslo: <input type="password" name="geslo" value="" required=""/></label></p>

        <p><button>Spremeni geslo</button></p>
    </form>
    </div>
<?php else: ?>
    <h1>Za uporabo te strani se je potrebno prijaviti</h1>   
    <p>[
    <a href="<?= BASE_URL . "prijavaAdmin" ?>">Prijava administratorja</a>
    ]</p>
<?php endif; ?>
    </div>
</body>