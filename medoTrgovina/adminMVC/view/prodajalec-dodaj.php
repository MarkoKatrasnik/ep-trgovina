

<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">

<style>
    body {
        margin:0;
    }

    ul.navigacija {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
        background-color: #4CAF50;
        position: fixed;
        top: 0;
        width: 100%;
    }

    li.navigacija {
        float: left;
    }

    a.navigacija{
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none; 
    }
    
    div.stran{
        padding:20px;
        margin-top:30px;
    }
  
     div.naslov {
         text-align: center;
     }
     
    button {
         width: 100%;
         background-color: #4CAF50;
         color: white;
         padding: 14px 20px;
         margin: 8px 0;
         border: none;
         border-radius: 4px;
         cursor: pointer;
     }

     button:hover {
         background-color: #45a049;
     }
     

    input[type=text], input[type=password], input[type=email], select {
        width: 100%;
        padding: 12px 20px;
        margin: 8px 0;
        display: inline-block;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
    }
    
    div.dodaj {
        border-radius: 5px;
        background-color: #f2f2f2;
        padding: 20px;
        margin-left: 300px;
        margin-right: 300px;
    }
    
</style>

<meta charset="UTF-8" />
<title>Nov prodajalec</title>


<body>
<?php
    if (isset($_SESSION["admin"])):
?>

     <ul class="navigacija">
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/adminMVC/domovAdmin">Domov</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/adminMVC/profilAdmin">Profil</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/adminMVC/uspesnaOdjava">Odjava</a></li>
    </ul>
    <div class="stran">
        <div class="naslov"><h1>Dodaj novega prodajalca</h1></div>
        <div class="dodaj">
        <form action="<?= BASE_URL . "dodajProdajalca"?>" method="post">
            <p><label>Ime: <input type="text" name="ime" value="<?= $ime ?>" autofocus required=""/></label></p>
            <p><label>Priimek: <input type="text" name="priimek" value="<?= $priimek ?>" required=""/></label></p>
            <p><label>Email: <input type="email" name="email" value="<?= $email ?>" required=""/> (Preverite, da se bo mail ujemal s tistim v certifikatu) </label></p> 
            <p><label>Geslo: <input type="password" name="geslo1" value="<?= $geslo1 ?>" required=""/></label></p>
            <p><label>Ponovi geslo: <input type="password" name="geslo2" value="<?= $geslo2 ?>" required=""/></label></p>
            <?php  
                if ($napaka) {?>
                    <p><label style="color:red"><?= $napaka ?> </label></p>
            <?php      }
            ?>
            <br>
            <p><label>Status: </label></p>
            <?php 
                if ($aktiviran == 0)  {?>
                    
            
            <table>
                <tr>
                 <td><input type="radio" name="aktiviran" value="aktiviran" > Prodajalec je aktiviran</td>
                 <td><input type="radio" name="aktiviran" value="deaktiviran" checked> Prodajalec je deaktiviran</td>
                 </tr>
             </table>
            <?php   

                }else{
                    ?>
            
              <table>
                <tr>
                 <td><input type="radio" name="aktiviran" value="aktiviran" checked> Prodajalec je aktiviran</td>
                    <td><input type="radio" name="aktiviran" value="deaktiviran" > Prodajalec je deaktiviran</td>
                    </tr>
             </table>
            <?php  
            }
            ?>

            <p><button>Dodaj novega prodajalca</button></p>
        </form>
        </div>
        <?php else: ?>
            <h1>Za uporabo te strani se je potrebno prijaviti</h1>   
            <p>[
            <a href="<?= BASE_URL . "prijavaAdmin" ?>">Prijava administratorja</a>
            ]</p>
        <?php endif; ?>
    </div>
</body>