

<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">

<style>
        body {
            margin:0;
        }
        
        ul.navigacija {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #4CAF50;
            position: fixed;
            top: 0;
            width: 100%;
        }
        
        li.navigacija {
            float: left;
        }
        
        a.navigacija{
                display: block;
                color: white;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none; 
        }
        
        div.stran{
            padding:20px;
            margin-top:30px;
        }
        
        input[type=text], input[type=password], input[type=email], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        button {
            width: 100%;
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        button:hover {
            background-color: #45a049;
        }

        div.urejanje {
            border-radius: 5px;
            background-color: #f2f2f2;
            padding: 20px;
            margin-left: 300px;
            margin-right: 300px;
        }
        
        div.naslov {
            text-align: center;
        }

        a.link{
        color: #4CAF50;
        /*text-decoration:none;*/
        }
        
    </style>     

<meta charset="UTF-8" />
<title>Uredi profil</title>

<body>
<?php
    if (isset($_SESSION["admin"])):
?>


     <ul class="navigacija">
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/adminMVC/domovAdmin">Domov</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/adminMVC/profilAdmin">Profil</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/adminMVC/uspesnaOdjava">Odjava</a></li>
    </ul>
    <div class="stran">
        <div class="naslov"><h1>Uredi profil</h1></div>
            <p>Želiš spremeniti geslo? <a class="link" href="<?= BASE_URL . "spremeniGesloAdmin" ?>">Spremeni geslo</a></p>   
    <div class="urejanje">
        <form action="<?= BASE_URL . "profilAdminUredi" ?>" method="post">
            <p><label>Ime: <input type="text" name="ime" value="<?= $items["ime"] ?>" autofocus required=""/></label></p>
            <p><label>Priimek: <input type="text" name="priimek" value="<?= $items["priimek"] ?>" required=""/></label></p>
            <p><label>Email: <input type="email" name="email" value="<?= $items["email"] ?>" required=""/> (Pozor! Nov email se ne bo ujemal s tistim v certifikatu) </label></p>

            <p><button>Posodobi podatke</button></p>
        </form>
    </div>

<?php else: ?>
    <h1>Za uporabo te strani se je potrebno prijaviti</h1>   
    <p>[
    <a href="<?= BASE_URL . "prijavaAdmin" ?>">Prijava administratorja</a>
    ]</p>
<?php endif; ?>
    </div>
</body>