
<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">
<style>
    .alert {
    padding: 20px;
    background-color: #f44336;
    color: white;
    width: 300px;
    }

    .closebtn {
        margin-left: 15px;
        color: white;
        font-weight: bold;
        float: right;
        font-size: 22px;
        line-height: 20px;
        cursor: pointer;
        transition: 0.3s;
    }

    .closebtn:hover {
        color: black;
    }
    body {
        margin:0;
    }

    ul.navigacija {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
        background-color: #4CAF50;
        position: fixed;
        top: 0;
        width: 100%;
    }

    li.navigacija {
        float: left;
    }

    a.navigacija{
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none; 
    }
    
    div.stran{
        padding:20px;
        margin-top:30px;
    }
    
    input[type=password]{
         width: 100%;
         padding: 12px 20px;
         margin: 8px 0;
         display: inline-block;
         border: 1px solid #ccc;
         border-radius: 4px;
         box-sizing: border-box;
     }

     button {
         width: 100%;
         background-color: #4CAF50;
         color: white;
         padding: 14px 20px;
         margin: 8px 0;
         border: none;
         border-radius: 4px;
         cursor: pointer;
     }

     button:hover {
         background-color: #45a049;
     }

     div.prijava {
         border-radius: 5px;
         background-color: #f2f2f2;
         padding: 20px;
         margin-left: 300px;
         margin-right: 300px;
     }

     div.naslov {
         text-align: center;
     }
     
       a.link{
        color: #4CAF50;
        /*text-decoration:none;*/
        }
     
</style>

<meta charset="UTF-8" />
<title>Prijava - administrator</title>

<body>
    <ul class="navigacija">
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/items">Trgovina</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/adminMVC/prijavaAdmin">Prijava</a></li>
        
    </ul>
    
    <div class="stran">
        <div class="naslov"><h1>Prijava - administrator</h1></div>
        <div class="prijava">
        <form action="<?= BASE_URL . "prijavaAdminPotrdi" ?>" method="post">

            <p><label>Email: <label ><?= $email ?> </label></label></p>
            <input type="hidden" name="email" value="<?= $email ?>" />
            <p><label>Geslo: <input type="password" name="geslo" value="<?= $geslo ?>" autocomplete="off"/></label></p>
            <?php if ($sporocilo != ""): ?>
                <div class="alert">
                    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
                    <p> <?= $sporocilo ?> </p>
                </div>
            <?php endif; ?>
            <p><button>Prijava</button></p>
        </form>
        </div>
    </div>
</body>