

<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">

<style>
    body {
        margin:0;
    }

    ul.navigacija {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
        background-color: #4CAF50;
        position: fixed;
        top: 0;
        width: 100%;
    }

    li.navigacija {
        float: left;
    }

    a.navigacija{
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none; 
    }
    
    div.stran{
        padding:20px;
        margin-top:30px;
    }
  
     div.naslov {
         text-align: center;
     }
     
    ul.seznam {
        background: #818285;
        padding: 20px;
        list-style-type: none;
        margin-left: auto; 
        margin-right: auto;
    }
     
    li.seznam {
        background: #f2f2f2;
        margin: 2px;
        padding: 20px;
    }
    
    a.seznam{
        color: #4CAF50;
        /*text-decoration:none;*/
    }
    
</style>

<meta charset="UTF-8" />
<title>Profil</title>
<body>
<?php
    if (isset($_SESSION["admin"])):
        
        ?>
   
        <ul class="navigacija">
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/adminMVC/domovAdmin">Domov</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/adminMVC/profilAdmin">Profil</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/adminMVC/uspesnaOdjava">Odjava</a></li>
    </ul>
    <div class="stran">
        <div class="naslov"><h1>Profil - administrator</h1></div>

        <p>Želiš urediti profil? 
        <a class="seznam" href="<?= BASE_URL . "profilAdminUredi" ?>">Uredi profil</a>
        </p>

        <ul class="seznam">
            <li class="seznam">Ime: <b><?= $items["ime"] ?> </b></li>
            <li class="seznam">Priimek: <b><?= $items["priimek"] ?> </b></li>
            <li class="seznam">E-mail: <b><?= $items["email"] ?> </b></li>
        </ul>


    <?php else: ?>
        <h1>Za uporabo te strani se je potrebno prijaviti</h1>   
        <p>[
        <a href="<?= BASE_URL . "prijavaAdmin" ?>">Prijava administratorja</a>
        ]</p>
    <?php endif; ?>
    </div>
</body>