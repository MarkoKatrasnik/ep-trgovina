<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">

<style>
/*table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}*/

 body {
        margin:0;
    }

    ul.navigacija {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
        background-color: #4CAF50;
        position: fixed;
        top: 0;
        width: 100%;
    }

    li.navigacija {
        float: left;
    }

    a.navigacija{
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none; 
    }
    
    div.stran{
        padding:20px;
        margin-top:30px;
    }
  
     div.naslov {
         text-align: center;
     }
     
    table{
        margin-left: auto;
        margin-right: auto;
        width: 100%;
        text-align: left;
        table-layout: fixed;
        background-color: #818285;
        padding:10px;
    } 
    th, td {
        /*padding: 5px;*/
        text-align: left;  
        padding: 10px;
        background: #f2f2f2;
    }
</style>

<meta charset="UTF-8" />
<title>Dnevnik - administrator</title>

<body>
<?php
    if (isset($_SESSION["admin"])):
?>

<ul class="navigacija">
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/adminMVC/domovAdmin">Domov</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/adminMVC/profilAdmin">Profil</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/adminMVC/uspesnaOdjava">Odjava</a></li>
    </ul>
    <div class="stran">
        <div class="naslov"><h1>Dnevnik - administrator</h1></div>


        <table>
          <tr>
            <th>Datum</th>
            <th>Aktivnost</th>
            <th>Podrobnosti</th>
          </tr>
          <?php foreach ($items as $item): ?>
              <tr>
                <td><?= $item["datum"] ?></td>
                <td><?= $item["aktivnost"] ?></td>
                <td><?= $item["podrobnosti"] ?></td>
              </tr>
          <?php endforeach; ?>

        </table>

        <?php else: ?>
            <h1>Za uporabo te strani se je potrebno prijaviti</h1>   
            <p>[
            <a href="<?= BASE_URL . "prijavaAdmin" ?>">Prijava administratorja</a>
            ]</p>
        <?php endif; ?>
    </div>
</body>