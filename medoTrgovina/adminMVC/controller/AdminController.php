<?php

require_once("../model/AdminDB.php");
require_once("../model/ShopDB.php");
require_once("ViewHelper.php");

class AdminController {
    public static function prijavaAdminAdd() {
        $data = filter_input_array(INPUT_POST, self::getRulesPrijava());
        #var_dump($data);
        if (self::checkValues($data)) {
            
            try {
                //pridobi podatke o uporabniku
                $uporabnik = AdminDB::getAdmin(["email" => $data["email"]]);

                if (password_verify($data["geslo"], $uporabnik["geslo"])) {
                    //session_start();
                    $_SESSION["admin"] = $uporabnik["id"];
                    
                    $dataDnevnik["admin_id"] = $uporabnik["id"];
                    $dataDnevnik["aktivnost"] = "Prijava";
                    $dataDnevnik["podrobnosti"] = "/";
                    $idDnevnik = AdminDB::insertDnevnik($dataDnevnik);
                    
                    echo ViewHelper::redirect(BASE_URL . "domovAdmin");
                }
                else {
                    $data["sporocilo"] = 'Geslo ni pravilno!';
                    self::prijavaAdminForm($data);
                }
            } catch (Exception $e) {
                die($e->getMessage());
                #self::prijavaAdminForm($data);
            }
        }
        else {
            self::prijavaAdminForm($data);
        }
    }

    

    public static function prijavaAdminForm($values = [
        "email" => "",
        "geslo" => "",
        "sporocilo" => ""
    ]) {
        session_regenerate_id();
        unset($_SESSION["stranka"]);
        unset($_SESSION["admin"]);
        unset($_SESSION["prodajalec"]);
        
        $client_cert = filter_input(INPUT_SERVER, "SSL_CLIENT_CERT");
        
        if ($client_cert == null) {
            die('err: Spremenljivka SSL_CLIENT_CERT ni nastavljena.');
        }

        $cert_data = openssl_x509_parse($client_cert);
        #var_dump($cert_data);
        
        $commonname = (is_array($cert_data['subject']['emailAddress']) ?
                        $cert_data['subject']['emailAddress'][0] : $cert_data['subject']['emailAddress']);
        $podjetje = ($cert_data['subject']['O']);
        $delovnoMesto = ($cert_data['subject']['OU']);
        $values["email"] = $commonname;
        
        
        if ($podjetje == 'Trgovina' && $delovnoMesto == 'Administrator') {
            try {
                //pridobi podatke o uporabniku
                $uporabnik = AdminDB::getAdmin(["email" => $values["email"]]);


            } catch (Exception $e) {
                #die($e->getMessage());
                $url = substr(BASE_URL, 0, -10);
                #var_dump($url);
                echo ViewHelper::redirect("$url/items");
            }

            echo ViewHelper::render("view/prijava-admin.php", $values);
        } else {
            #echo("Podatki: $podjetje, $delovnoMesto, $commonname");

            $url = substr(BASE_URL, 0, -10);
            echo ViewHelper::redirect("$url/items"); # preusmeri na stran za izbiro med prijavo kot stranka ali prodajalec ali administrator, po možnosti opozori,
                                                    # po možnosti opozori, da certifikat ni bil pravilen
        }
        
    }
    
    public static function domov() {
        echo ViewHelper::render("view/domov-admin.php");
    }
    
    public static function profilAdmin() {
        if (isset($_SESSION["admin"])) {
            $id = $_SESSION["admin"];
            echo ViewHelper::render("view/profil-admin.php", [
                "items" => AdminDB::getProfil(["id" => $id])
            ]);
        }
        else {
            echo ViewHelper::render("view/profil-admin.php");
        }
    }
    
    public static function profilAdminUrediForm() {
        $id = $_SESSION["admin"];
        echo ViewHelper::render("view/profil-uredi-admin.php", [
            "items" => AdminDB::getProfil(["id" => $id])
        ]);
    }
    
    public static function profilAdminUrediIzvedi() {
        
        $data = filter_input_array(INPUT_POST, self::getRulesUrediProfilA());
        $data["id"] = $_SESSION["admin"];

        if (self::checkValues($data)) {
            //$idPrijavljen = $_SESSION["stranka"];
            
            $id = AdminDB::updateProfil($data);
            
            $dataDnevnik["admin_id"] = $data["id"];
            $dataDnevnik["aktivnost"] = "Urejanje lastnega profila";
            $dataDnevnik["podrobnosti"] = "/";
            $idDnevnik = AdminDB::insertDnevnik($dataDnevnik);
            
            echo ViewHelper::redirect(BASE_URL . "profilAdmin");
        } else {
            self::profilAdminUrediForm($data);
        }
    }
    
    public static function spremeniGesloForm($values = [
        "geslo" => ""
    ]) {
        echo ViewHelper::render("view/spremeni-geslo-admin.php", $values);
    }
    
    public static function spremeniGesloIzvedi() {
        
        $data = filter_input_array(INPUT_POST, self::getRulesSpremeniGeslo());
        $data["id"] = $_SESSION["admin"];

        if (self::checkValues($data)) {
            $data["geslo"] = password_hash($data["geslo"], PASSWORD_DEFAULT);
            $id = AdminDB::updateGeslo($data);
            
            $dataDnevnik["admin_id"] = $data["id"];
            $dataDnevnik["aktivnost"] = "Sprememba lastnega gesla";
            $dataDnevnik["podrobnosti"] = "/";
            $idDnevnik = AdminDB::insertDnevnik($dataDnevnik);
            
            echo ViewHelper::redirect(BASE_URL . "profilAdmin");
        } else {
            self::spremeniGesloForm($data);
        }
    }
    
    public static function ustvariProdajalec(){
        $data = filter_input_array(INPUT_POST, self::getRulesUstvariProdajalec());
        
        if (self::checkValues($data)) {
            if ($data["geslo1"] == $data["geslo2"]) {
                $data["geslo"] = password_hash($data["geslo1"], PASSWORD_DEFAULT);
                //$idPrijavljen = $_SESSION["stranka"];
                $id = AdminDB::createProdajalec($data);
                
                $dataDnevnik["admin_id"] = $_SESSION["admin"];
                $dataDnevnik["aktivnost"] = "Ustvarjen nov profil prodajalca";
                $dataDnevnik["podrobnosti"] = $data["ime"] . " " . $data["priimek"];
                $idDnevnik = AdminDB::insertDnevnik($dataDnevnik);
                
                echo ViewHelper::redirect(BASE_URL . "prodajalciAdmin");
            } else {
                $data["napaka"] = "Gesli se ne ujemata!";
                self::novProdajalecForm($data);
            }
        } else {
            self::prodajalecUrediForm($data);
        }
    }
    
    public static function novProdajalecForm($values = [
        "ime" => "",
        "priimek" => "",
        "email" => "",
        "geslo1" => "", 
        "geslo2" => "", 
        "aktiviran" => 1,
        "napaka" => null
    ]){
        echo ViewHelper::render("view/prodajalec-dodaj.php", $values);
    }
    
    public static function prodajalciList() {
        echo ViewHelper::render("view/seznam-prodajalci.php", [
            "items" => AdminDB::getProdajalciList()
        ]);
    }
    
    public static function prodajalecUrediForm($id) {
        echo ViewHelper::render("view/prodajalec-uredi.php", [
            "items" => AdminDB::getProdajalec(["id" => $id])
        ]);
    }
    
    public static function prodajalecUrediIzvedi($id) {
       
        $data = filter_input_array(INPUT_POST, self::getRulesUrediProfilP());
        $data["id"] = $id;
            if (self::checkValues($data)) {
            //$idPrijavljen = $_SESSION["stranka"];
            
            $id = AdminDB::updateProdajalecProfil($data);
            
            $dataDnevnik["admin_id"] = $_SESSION["admin"];
            $dataDnevnik["aktivnost"] = "Sprememba profila prodajalca";
            $dataDnevnik["podrobnosti"] = $data["ime"] . " " . $data["priimek"];
            $idDnevnik = AdminDB::insertDnevnik($dataDnevnik);
            
            echo ViewHelper::redirect(BASE_URL . "prodajalciAdmin");
        } else {
            
            self::prodajalecUrediForm($data["id"]);
        }
    }
    
    
    public static function spremeniGesloProdajalecForm($id, $values = [
        "geslo" => ""
    ]) {
        
        $values["id"] = $id;
        #var_dump($values);
        echo ViewHelper::render("view/spremeni-geslo-prodajalec-admin.php", ["items" => ['id' => $id]]);
    }
    
    public static function spremeniGesloProdajalecIzvedi($id) {
        
        $data = filter_input_array(INPUT_POST, self::getRulesSpremeniGeslo());
        $data["id"] = $id;

        if (self::checkValues($data)) {
            $data["geslo"] = password_hash($data["geslo"], PASSWORD_DEFAULT);
            $id = AdminDB::updateProdajalecGeslo($data);
            
            $dataDnevnik["admin_id"] = $_SESSION["admin"];
            $dataDnevnik["aktivnost"] = "Sprememba gesla prodajalca";
            $dataDnevnik["podrobnosti"] = "Id prodajalca: " . $data["id"];
            $idDnevnik = AdminDB::insertDnevnik($dataDnevnik);
            
            echo ViewHelper::redirect(BASE_URL . "prodajalciAdmin");
        } else {
            self::spremeniGesloProdajalecForm($data);
        }
    }
    
    
    
    public static function dodajArtikelAdd() {
        
        $data = filter_input_array(INPUT_POST, self::getRulesDodajArtikel());
        
        if (2>1) {
            
            $imgFile = $_FILES["slika"]["name"];
            $tmp_dir = $_FILES["slika"]['tmp_name'];
            $imgSize = $_FILES["slika"]['size'];
            
            $upload_dir = '../static/images/'; // upload directory
 
            $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension

            // valid image extensions
            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions

            // rename uploading image
            $userpic = rand(1000,1000000).".".$imgExt;
            
            // allow valid image file formats
            if(in_array($imgExt, $valid_extensions)){   
             // Check file size '5MB'
             if($imgSize < 5000000)    {
              move_uploaded_file($tmp_dir,$upload_dir.$userpic);
             }
             else{
              $errMSG = "Sorry, your file is too large.";
             }
            }
            else{
             $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";  
            }
            $data["slika2"] = $userpic;
            var_dump($data);
            if(!isset($errMSG)) {
                $id = ProdajalecDB::dodajArtikel($data);
                echo ViewHelper::redirect(BASE_URL . "dodajArtikel"); //treba zamenjati
            }
            
            
        } else {
            self::dodajArtikelForm($data);
        }
    }
    
    public static function artikelUrediForm($id) {
        
        echo ViewHelper::render("view/uredi-artikel.php", [
            "items" => ShopDB::get(["id" => $id])
        ]);
    }
    
    public static function artikelUrediIzvedi() {
        $data = filter_input_array(INPUT_POST, self::getRulesUrediArtikel());
        
        $imgFile = $_FILES["slika"]["name"];
        $tmp_dir = $_FILES["slika"]['tmp_name'];
        $imgSize = $_FILES["slika"]['size'];
        
        if($imgFile){
         $upload_dir = '../static/images/'; // upload directory
         $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
         $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
         $userpic = rand(1000,1000000).".".$imgExt;
         if(in_array($imgExt, $valid_extensions))
         {   
          if($imgSize < 5000000)
          {
           unlink($upload_dir.$data['slk']);
           move_uploaded_file($tmp_dir,$upload_dir.$userpic);
           $data["slika"] = "185241.jpg";
           $data["slika2"] = $userpic;
          }
          else
          {
           $errMSG = "Sorry, your file is too large it should be less then 5MB";
          }
         }
         else
         {
          $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";  
         } 
        }
        else
        {
         // if no image selected the old image remain as it is.
         $data["slika2"] = $data["slk"]; // old image from database
         $data["slika"] = "185241.jpg";
        }
        var_dump($data);
        
        if (self::checkValues($data)) {
            //$idPrijavljen = $_SESSION["stranka"];
            
            $id = $data["id"];
            $id2 = ProdajalecDB::updateArtikel($data);
            echo ViewHelper::redirect(BASE_URL . "urediArtikel/$id"); //treba zamenjati
        } else {
            self::artikelUrediForm($data);
        }
    }
    
    public static function dnevnik() {
        if (isset($_SESSION["admin"])) {
            $id = $_SESSION["admin"];
            echo ViewHelper::render("view/dnevnik-admin.php", [
                "items" => AdminDB::getDnevnik(["admin_id" => $id])
            ]);
        }
        else {
            echo ViewHelper::render("view/dnevnik-admin.php");
        }
    }
    
    public static function dnevnikProdajalec($idProdajalca) {
        
        echo ViewHelper::render("view/dnevnik-prodajalec-admin.php", [
            "items" => AdminDB::getDnevnikProdajalec(["prodajalec_id" => $idProdajalca])
        ]);
    }
    
    public static function uspesnaOdjava() {
        unset($_SESSION["admin"]);
        echo ViewHelper::render("view/uspesnaOdjava-admin.php");
    }

    public static function checkValues($input) {
        if (empty($input)) {
            return FALSE;
        }

        $result = TRUE;
        foreach ($input as $value) {
            $result = $result && $value != false;
        }

        return $result;
    }
    
    
    public static function getRulesDodajArtikel() {
        return [
            'naziv' => FILTER_SANITIZE_SPECIAL_CHARS,
            'opis' => FILTER_SANITIZE_SPECIAL_CHARS,
            'cena' => FILTER_SANITIZE_SPECIAL_CHARS,
            'aktiviran' => FILTER_SANITIZE_SPECIAL_CHARS,
            'ocena' => FILTER_SANITIZE_SPECIAL_CHARS,
            'slika' => FILTER_SANITIZE_SPECIAL_CHARS,
            'stOcen' => FILTER_SANITIZE_SPECIAL_CHARS
        ];
    }
    
    public static function getRulesUrediArtikel() {
        return [
            'naziv' => FILTER_SANITIZE_SPECIAL_CHARS,
            'opis' => FILTER_SANITIZE_SPECIAL_CHARS,
            'cena' => FILTER_SANITIZE_SPECIAL_CHARS,
            'id' => FILTER_SANITIZE_SPECIAL_CHARS,
            'slika' => FILTER_SANITIZE_SPECIAL_CHARS,
            'slk' => FILTER_SANITIZE_SPECIAL_CHARS
        ];
    }
    
    public static function getRulesPrijava() {
        return [
            'email' => FILTER_SANITIZE_SPECIAL_CHARS,
            'geslo' => FILTER_SANITIZE_SPECIAL_CHARS
        ];
    }
    
    public static function getRulesUstvariProdajalec() {
        return [
            'ime' => FILTER_SANITIZE_SPECIAL_CHARS,
            'priimek' => FILTER_SANITIZE_SPECIAL_CHARS,
            'email' => FILTER_SANITIZE_SPECIAL_CHARS,
            'geslo1' => FILTER_SANITIZE_SPECIAL_CHARS,
            'geslo2' => FILTER_SANITIZE_SPECIAL_CHARS,
            'aktiviran' => FILTER_SANITIZE_SPECIAL_CHARS
        ];
    }
    
    public static function getRulesUrediProfilA() {
        return [
            'ime' => FILTER_SANITIZE_SPECIAL_CHARS,
            'priimek' => FILTER_SANITIZE_SPECIAL_CHARS,
            'email' => FILTER_SANITIZE_SPECIAL_CHARS
        ];
    }
    
    public static function getRulesUrediProfilP() {
        return [
            'ime' => FILTER_SANITIZE_SPECIAL_CHARS,
            'priimek' => FILTER_SANITIZE_SPECIAL_CHARS,
            'email' => FILTER_SANITIZE_SPECIAL_CHARS,
            'aktiviran' => FILTER_SANITIZE_SPECIAL_CHARS
        ];
    }
    
    public static function getRulesSpremeniGeslo() {
        return [
            'geslo' => FILTER_SANITIZE_SPECIAL_CHARS
        ];
    }
}
