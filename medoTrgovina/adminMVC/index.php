<?php

// enables sessions for the entire app
session_start();

require_once("controller/AdminController.php");

define("BASE_URL", rtrim($_SERVER["SCRIPT_NAME"], "index.php"));
define("IMAGES_URL", rtrim($_SERVER["SCRIPT_NAME"], "index.php") . "static/images/");
define("CSS_URL", rtrim($_SERVER["SCRIPT_NAME"], "index.php") . "static/css/");

$path = isset($_SERVER["PATH_INFO"]) ? trim($_SERVER["PATH_INFO"], "/") : "";

$urls = [
    "/^$/" => function () {
        ViewHelper::redirect(BASE_URL . "prijavaAdmin");
    },
            
            
    # ADMINISTRATOR
    "/^prijavaAdmin$/" => function ($method) {
        AdminController::prijavaAdminForm();
       
    },
    
    "/^prijavaAdminPotrdi$/" => function ($method) {
        if ($method == "POST") {
            AdminController::prijavaAdminAdd();
        } 
       
    },      
            
            
    "/^domovAdmin$/" => function ($method) {
        AdminController::domov();
    },
            
    "/^profilAdmin$/" => function ($method) {
        AdminController::profilAdmin();
    },
            
    "/^profilAdminUredi$/" => function ($method) {
        if ($method == "POST") {
            AdminController::profilAdminUrediIzvedi();
        } else {
            AdminController::profilAdminUrediForm();
        }
    }, 
            
    "/^spremeniGesloAdmin$/" => function ($method) {
        if ($method == "POST") {
            AdminController::spremeniGesloIzvedi();
        } else {
            AdminController::spremeniGesloForm();
        }
    },
            
    "/^prodajalciAdmin\/?(\d+)?$/" => function ($method, $id = null) {
        if ($id == null) {
            AdminController::prodajalciList();
        } else {
            
            if ($method == "POST") {
                AdminController::prodajalecUrediIzvedi($id);
            }            
            else {
                AdminController::prodajalecUrediForm($id);
            }
        }
    },
            
    "/^dodajProdajalca\/?(\d+)?$/" => function ($method, $id = null) {
        if ($method == "POST") {
            AdminController::ustvariProdajalec();
        } else {
            AdminController::novProdajalecForm();
        }
    },
            
    "/^spremeniGesloProdajalecAdmin\/?(\d+)?$/" => function ($method, $id = null) {
        if ($method == "POST") {
            AdminController::spremeniGesloProdajalecIzvedi($id);
        } else {
            AdminController::spremeniGesloProdajalecForm($id);
        }
    },
            
    "/^dnevnik$/" => function ($method) {
        AdminController::dnevnik();
    },
            
    "/^dnevnikProdajalec\/?(\d+)?$/" => function ($method, $id = null) {
        AdminController::dnevnikProdajalec($id);
    },
            
    "/^uspesnaOdjava$/" => function ($method) {
        AdminController::uspesnaOdjava();
    },
    
];

foreach ($urls as $pattern => $controller) {
    if (preg_match($pattern, $path, $params)) {
        try {
            $params[0] = $_SERVER["REQUEST_METHOD"];
            $controller(...$params);
        } catch (InvalidArgumentException $e) {
            ViewHelper::error404();
        } catch (Exception $e) {
            ViewHelper::displayError($e, true);
        }

        exit();
    }
}

ViewHelper::displayError(new InvalidArgumentException("No controller matched."), true);
