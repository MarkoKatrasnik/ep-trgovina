<?php

require_once("./model/ShopDB.php");
require_once("./model/StrankaDB.php");
require_once("ViewHelper.php");

class ShopRESTController {

    public static function get($id) {
        try {
            echo ViewHelper::renderJSON(ShopDB::get(["id" => $id]));
        } catch (InvalidArgumentException $e) {
            echo ViewHelper::renderJSON($e->getMessage(), 404);
        }
    }

    public static function index() {
        $prefix = $_SERVER["REQUEST_SCHEME"] . "://" . $_SERVER["HTTP_HOST"]
                . $_SERVER["REQUEST_URI"] . "/";
        echo ViewHelper::renderJSON(ShopDB::getAllwithURI(["prefix" => $prefix]));
    }
    
    public static function prijava($email) {
        $data["email"] = $email;
        $data = filter_var_array($data, self::getRulesPreveriEmail());
        try {
            echo ViewHelper::renderJSON(StrankaDB::getUser(["email" => $email]));
        } catch (InvalidArgumentException $e) {
            echo ViewHelper::renderJSON($e->getMessage(), 404);
        }
    }
    
    public static function profil($id) {
        try {
            echo ViewHelper::renderJSON(StrankaDB::getProfil(["id" => $id]));
        } catch (InvalidArgumentException $e) {
            echo ViewHelper::renderJSON($e->getMessage(), 404);
        }
    }
    
    public static function urediProfil($id) {
        // spremenljivka $_PUT ne obstaja, zato jo moremo narediti sami
        $_PUT = [];
        parse_str(file_get_contents("php://input"), $_PUT);
        $data = filter_var_array($_PUT, self::getRulesUrediProfil());

        if (self::checkValues($data)) {
            $data["id"] = $id;
            $data["postnaSt"] = substr($data["posta"], 0, 4);
            StrankaDB::updateProfil($data);
            echo ViewHelper::renderJSON("", 200);
        } else {
            echo ViewHelper::renderJSON("Missing data.", 400);
        }
    }
    
    public static function spremeniGeslo($id) {
        // spremenljivka $_PUT ne obstaja, zato jo moremo narediti sami
        $_PUT = [];
        parse_str(file_get_contents("php://input"), $_PUT);
        $data = filter_var_array($_PUT, self::getRulesSpremeniGeslo());
        
        if (self::checkValues($data)) {
            $data["id"] = $id;
            $id = StrankaDB::updateGeslo($data);
            echo ViewHelper::renderJSON("", 200);
        } else {
            echo ViewHelper::renderJSON("Missing data.", 400);
        }

    }
    
    public static function vsaNarocilaStranke($id) {
        $stranka_id = $id;
        $items["stranka_id"] = $stranka_id;
        try {
            echo ViewHelper::renderJSON(StrankaDB::getNarocilo($items));
        } catch (InvalidArgumentException $e) {
            echo ViewHelper::renderJSON($e->getMessage(), 404);
        }
    }
    
    public static function podrobnostiNarocila($id) {
        try {
            echo ViewHelper::renderJSON(StrankaDB::getNarociloPodrobnosti(["id" => $id]));
        } catch (InvalidArgumentException $e) {
            echo ViewHelper::renderJSON($e->getMessage(), 404);
        }
    }
    
    public static function pridobiKosarico($id) {
        $stranka_id = $id;
        $items["stranka_id"] = $stranka_id;
        try {
            //echo ($items["stranka_id"]);
            echo ViewHelper::renderJSON(StrankaDB::pridobiKosarico($items));
        } catch (InvalidArgumentException $e) {
            echo ViewHelper::renderJSON($e->getMessage(), 404);
        }
    }
    
    public static function izprazniKosarico($id) {
        $stranka_id = $id;
        $items["stranka_id"] = $stranka_id;
        try {
            //echo ($items["stranka_id"]);
            echo ViewHelper::renderJSON(StrankaDB::izprazniKosarico($items));
        } catch (InvalidArgumentException $e) {
            echo ViewHelper::renderJSON($e->getMessage(), 404);
        }
    }
    
    public static function posodobiKosarico($id) {
        $stranka_id = $id;
        $data = [];
        parse_str(file_get_contents("php://input"), $data);
            
        $data1 = $data["artikli"];
        $data2 = json_decode($data1, true);
       
        for ($i = 0; $i < count($data2); $i++) {

            $data3 = $data2[$i];
            $data3["stranka_id"] = $stranka_id;
            $podatki = filter_var_array($data3, self::getRulesPreveriPosodabljanje());
            //$test = $podatki["artikel_id"];
            //echo ViewHelper::renderJSON("ID $test", 400);
            if (self::checkValues($podatki["artikel_id"]) && self::checkValues($podatki["stranka_id"])) {

                try {
                    if ($podatki["stArtiklov"] == 0) {
                        // odstrani artikel iz kosarice
                        StrankaDB::odstraniIzKosarice($podatki);

                    } else {
                        //preveri, ce je artikel v kosarici
                        $vsebujeArtikel = StrankaDB::preveriKosarico($podatki);
                        if ($vsebujeArtikel) {
                            StrankaDB::posodobiKosarico($podatki);
                        }
                        else {
                            StrankaDB::dodajVKosaricoVec($podatki);
                        }
                    }

                }
                catch (Exception $e){
                    die($e->getMessage());
                }
            } else {
                echo ViewHelper::renderJSON("Missing data", 400);
            }


        }
        
    }
    
    public static function zakljuciNakup($id) {
        $stranka_id = $id;
        
        $data0["status"] = "Oddano";
        $data0["stranka_id"] = $stranka_id;
        $idNarocilo = StrankaDB::insertNarocilo($data0);
        
        $data = [];
        parse_str(file_get_contents("php://input"), $data);
            
        $data1 = $data["nakup"];
        $data2 = json_decode($data1, true);
        $count = count($data2);
        for ($i = 0; $i < count($data2); $i++) {
            $data3 = $data2[$i];
            $data3["narocilo_id"] = $idNarocilo;
            
            $podatki = filter_var_array($data3, self::getRulesPreveriVpisNakupa());
            //$test = $podatki["artikel_id"];
            //echo ViewHelper::renderJSON("ID $test", 400);
            if (self::checkValues($podatki)) {

                try {
                    $idPovezava = StrankaDB::insertPovezava($podatki);
                } catch (Exception $e){
                    die($e->getMessage());
                }
            } else {
                echo ViewHelper::renderJSON("Missing data", 400);
            }


        }
        StrankaDB::izprazniKosarico($data0);
        echo ViewHelper::renderJSON("", 200);
      
    }
    
    
    public static function dodajIzdelek() {
        
        $data = filter_input_array(INPUT_POST, self::getRulesPreveriID());
        
        if (self::checkValues($data)) {
            try {
                $vsebujeArtikel = StrankaDB::preveriKosarico($data);
                if ($vsebujeArtikel) {
                    $data["stArtiklov"] = $vsebujeArtikel[0]["stArtiklov"]+1;
                    StrankaDB::posodobiKosarico($data);
                    echo ViewHelper::renderJSON("", 200);
                }
                else {
                    StrankaDB::dodajVKosarico($data);
                    echo ViewHelper::renderJSON("", 200);
                }
                
            } catch (Exception $exc) {
                die($exc->getMessage());
            }
        
        } else {
            echo ViewHelper::renderJSON("Missing data", 400);
        }
        
    }

    
    
    
    
    
    
    
    
    public static function checkValues($input) {
        if (empty($input)) {
            return FALSE;
        }

        $result = TRUE;
        foreach ($input as $value) {
            $result = $result && $value != false;
        }

        return $result;
    }
    
    public static function getRulesPreveriID() {
        return [
            'artikel_id' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0]
            ], 
            'stranka_id' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0]
            ]
            
        ];
    }
    
    public static function getRulesSpremeniGeslo() {
        return [
            'geslo' => FILTER_SANITIZE_SPECIAL_CHARS
        ];
    }
    
    public static function getRulesPreveriPosodabljanje() {
        return [
            'artikel_id' => FILTER_SANITIZE_SPECIAL_CHARS,
            'stArtiklov' => FILTER_SANITIZE_SPECIAL_CHARS,
            'stranka_id' => FILTER_SANITIZE_SPECIAL_CHARS
        ];
    }
    
    public static function getRulesPreveriVpisNakupa() {
        return [
            'artikel_id' => FILTER_SANITIZE_SPECIAL_CHARS,
            'stArtiklov' => FILTER_SANITIZE_SPECIAL_CHARS,
            'narocilo_id' => FILTER_SANITIZE_SPECIAL_CHARS
        ];
    }
    
    
    public static function getRulesPreveriEmail() {
        return [
            'email' => FILTER_SANITIZE_SPECIAL_CHARS
        ];
    }
    
    
    public static function getRulesUrediProfil() {
        return [
            'ime' => FILTER_SANITIZE_SPECIAL_CHARS,
            'priimek' => FILTER_SANITIZE_SPECIAL_CHARS,
            'email' => FILTER_SANITIZE_SPECIAL_CHARS,
            'telSt' => FILTER_SANITIZE_SPECIAL_CHARS,
            'naslov' => FILTER_SANITIZE_SPECIAL_CHARS,
            'posta' => FILTER_SANITIZE_SPECIAL_CHARS
        ];
    }
    
}
