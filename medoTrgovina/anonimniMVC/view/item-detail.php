<?php header('X-Frame-Options: SAMEORIGIN'); ?>

<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">

<style>
    body {
        margin:0;
    }

    ul.navigacija {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
        background-color: #4CAF50;
        position: fixed;
        top: 0;
        width: 100%;
    }

    li.navigacija {
        float: left;
    }

    a.navigacija{
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none; 
    }
    
    div.stran{
        padding:20px;
        margin-top:30px;
    }
  
     div.naslov {
         text-align: center;
     }
     
    table{
        margin-left: auto;
        margin-right: auto;
        background-color: #818285;
        padding:10px;
    } 

    th, td {
        /*padding: 5px;*/
        text-align: left;  
        padding: 10px;
        background: #f2f2f2;
    }

     
</style>
<meta charset="UTF-8" />
<title>Podrobnosti</title>
<body>
    
    <ul class="navigacija">
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/items">Domov</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/strankaMVC/prijava">Prijava</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/strankaMVC/registracija">Registracija</a></li>
    </ul>
    
    <div class="stran">
        <div class="naslov"><h1><?= $naziv ?></h1></div>

        <table>
          <tr>
            <td rowspan="3"><img src="../static/images/<?= $slika ?>" alt="<?= $slika ?>" width="300px"></td>
            <td>Cena: <b><?= $cena ?> EUR</b></td>
          </tr>
          <tr>
            <td>Ocena: <i><?=$ocena == NULL ? "Artikel še ni ocenjen" : round($ocena, 1) ?></i></td>
          </tr>
          <tr>
              <td>Opis: <i><?= $opis ?></i></td>
          </tr>
        </table>
    </div>
</body>