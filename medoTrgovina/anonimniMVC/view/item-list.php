<?php header('X-Frame-Options: SAMEORIGIN'); ?>

<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">

<style>
    body {
        margin:0;
    }

    ul.navigacija {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
        background-color: #4CAF50;
        position: fixed;
        top: 0;
        width: 100%;
    }

    li.navigacija {
        float: left;
    }

    a.navigacija{
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none; 
    }
    
    div.stran{
        padding:20px;
        margin-top:30px;
    }
  
     div.naslov {
         text-align: center;
     }
     
     ul.seznam {
        background: #818285;
        padding: 20px;
        list-style-type: none;
    }
    
    li.seznam {
        background: #f2f2f2;
        margin: 2px;
        padding: 20px;
    }
    
    a.seznam{
        color: #4CAF50;
        /*text-decoration:none;*/
    }
    
</style>

<meta charset="UTF-8" />
<title>Vsi izdelki</title>

<body>
    
    <ul class="navigacija">
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/items">Domov</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/strankaMVC/prijava">Prijava</a></li>
        <li class="navigacija"><a class="navigacija" href="/netbeans/ep-trgovina/medoTrgovina/strankaMVC/registracija">Registracija</a></li>
    </ul>
    <div class="stran">
        <div class="naslov"><h1>Vsi izdelki</h1></div>

        <ul class="seznam">

            <?php foreach ($items as $item): ?>
                <li class="seznam"><a class="seznam" href="<?= BASE_URL . "items/" . $item["id"] ?>"><?= $item["naziv"] ?> (<?= $item["cena"] ?> EUR)</a></li>
            <?php endforeach; ?>

        </ul>
    </div>

</body>