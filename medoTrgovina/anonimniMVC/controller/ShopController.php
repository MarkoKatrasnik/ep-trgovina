<?php

require_once("./model/ShopDB.php");
require_once("ViewHelper.php");

class ShopController {

    public static function get($id) {
        echo ViewHelper::render("anonimniMVC/view/item-detail.php", ShopDB::get(["id" => $id]));
    }

    public static function index() {
        echo ViewHelper::render("anonimniMVC/view/item-list.php", [
            "items" => ShopDB::getAll()
        ]);
    }
}
