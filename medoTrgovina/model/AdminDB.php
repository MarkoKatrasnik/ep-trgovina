<?php

require_once 'AbstractDB.php';

class AdminDB extends AbstractDB {

    public static function insert(array $params) {
        return parent::modify("INSERT INTO stranka (ime, priimek, email, geslo, naslov, telSt) "
                        . " VALUES (:ime, :priimek, :email, :geslo, :naslov, :telSt)", $params);
    }

    public static function get(array $id) {
        $books = parent::query("SELECT id, naziv, opis, cena"
                        . " FROM artikel"
                        . " WHERE id = :id", $id);

        if (count($books) == 1) {
            return $books[0];
        } else {
            throw new InvalidArgumentException("Ni artikla s tem id");
        }
    }
    
    public static function getAdmin(array $email) {
        $admini = parent::query("SELECT id, email, geslo"
                        . " FROM administrator"
                        . " WHERE email = :email", $email);

        if (count($admini) == 1) {
            return $admini[0];
        } else {
            #return null;
            throw new InvalidArgumentException("Ni administratorja s takim emailom");
        }
    }
    
    public static function getProfil(array $id) {
        $stranke = parent::query("SELECT *"
                        . " FROM administrator"
                        . " WHERE id = :id", $id);

        if (count($stranke) == 1) {
            return $stranke[0];
        } else {
            throw new InvalidArgumentException("Ni administratorja s takim id-jem");
        }
    }
    
    public static function getProdajalciList() {
        return parent::query("SELECT id, ime, priimek"
                        . " FROM prodajalec"
                        . " ORDER BY id ASC");
    }
    
    public static function getProdajalec(array $id) {
        $stranke = parent::query("SELECT *"
                        . " FROM prodajalec"
                        . " WHERE id = :id", $id);

        if (count($stranke) == 1) {
            return $stranke[0];
        } else {
            throw new InvalidArgumentException("Ni prodajalca s takim emailom");
        }
    }
    
    public static function updateProdajalecProfil(array $params) {
        if ($params["aktiviran"] == 'aktiviran') {
            $params["aktiviran"] = 1;
        } else {
            $params["aktiviran"] = 0;
        }
        return parent::modify("UPDATE prodajalec SET ime = :ime, priimek = :priimek, "
                        . "email = :email, aktiviran= :aktiviran"
                        . " WHERE id = :id", $params);
    }
    
    public static function createProdajalec(array $params) {
        if ($params["aktiviran"] == 'aktiviran') {
            $params["aktiviran"] = 1;
        } else {
            $params["aktiviran"] = 0;
        }
        return parent::modify("INSERT INTO prodajalec (ime, priimek, email, geslo, aktiviran) "
                        . " VALUES (:ime, :priimek, :email, :geslo, :aktiviran)", $params);
    }
 
    
    public static function insertNarocilo(array $params) {
        return parent::modify("INSERT INTO narocilo (status, stranka_id, datum) "
                        . " VALUES (:status, :stranka_id, NOW())", $params);
    }
    
    public static function insertPovezava(array $params) {
        return parent::modify("INSERT INTO artikel_has_narocilo (artikel_id, narocilo_id, stArtiklov) "
                        . " VALUES (:artikel_id, :narocilo_id, :stArtiklov)", $params);
    }

    public static function getAll() {
        return parent::query("SELECT id, naziv, cena"
                        . " FROM artikel"
                        . " ORDER BY id ASC");
    }
    
    public static function updateProfil(array $params) {
        return parent::modify("UPDATE administrator SET ime = :ime, priimek = :priimek, "
                        . "email = :email"
                        . " WHERE id = :id", $params);
    }
    
    public static function updateGeslo(array $params) {
        return parent::modify("UPDATE administrator SET geslo = :geslo"
                        . " WHERE id = :id", $params);
    }
    
    public static function updateProdajalecGeslo(array $params) {
        return parent::modify("UPDATE prodajalec SET geslo = :geslo"
                        . " WHERE id = :id", $params);
    }

    public static function getAllwithURI(array $prefix) {
        return parent::query("SELECT id, author, title, price, year, "
                        . "          CONCAT(:prefix, id) as uri "
                        . "FROM book "
                        . "ORDER BY id ASC", $prefix);
    }
    
    public static function getNarocilo(array $stranka_id) {
        return parent::query("SELECT *"
                        . " FROM narocilo"
                        . " WHERE stranka_id = :stranka_id", $stranka_id);
    }
    
    public static function getNarociloPodrobnosti(array $id) {
        return parent::query("select narocilo.stranka_id, artikel_has_narocilo.narocilo_id, artikel.naziv, artikel.cena, artikel_has_narocilo.stArtiklov, narocilo.status, narocilo.datum FROM narocilo INNER JOIN artikel_has_narocilo ON narocilo.id = artikel_has_narocilo.narocilo_id INNER JOIN artikel ON artikel_has_narocilo.artikel_id = artikel.id WHERE narocilo.id = :id", $id);
    }
    
    public static function getArtikelOcena(array $id) {
        $books = parent::query("SELECT ocena, stOcen"
                        . " FROM artikel"
                        . " WHERE id = :id", $id);

        if (count($books) == 1) {
            return $books[0];
        } else {
            throw new InvalidArgumentException("No such book");
        }
    }
    
    public static function posodobiOceno(array $params) {
        return parent::modify("UPDATE artikel SET ocena = :novaOcena, stOcen = :stOcen"
                        . " WHERE id = :id", $params);
    }
    
    public static function insertDnevnik(array $params) {
        return parent::modify("INSERT INTO dnevnikAdmin (admin_id, datum, aktivnost, podrobnosti) "
                        . " VALUES (:admin_id, NOW(), :aktivnost, :podrobnosti)", $params);
    }
    
    public static function getDnevnik(array $admin_id) {
        return parent::query("SELECT *"
                        . " FROM dnevnikAdmin"
                        . " WHERE admin_id = :admin_id", $admin_id);
    }
    
    public static function getDnevnikProdajalec(array $prodajalec_id) {
        return parent::query("SELECT *"
                        . " FROM dnevnikProdajalec"
                        . " WHERE prodajalec_id = :prodajalec_id", $prodajalec_id);
    }
}