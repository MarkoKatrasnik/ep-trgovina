<?php

class dodajTabele {
    function sqlPoizvedba() {
        $sql = "
        -- -----------------------------------------------------
        -- Table `trgovina`.`artikel`
        -- -----------------------------------------------------
        CREATE TABLE IF NOT EXISTS `trgovina`.`artikel` (
          `id` INT NOT NULL AUTO_INCREMENT,
          `naziv` VARCHAR(45) NOT NULL,
          `opis` VARCHAR(255) NOT NULL,
          `cena` DOUBLE NOT NULL,
          `aktiviran` TINYINT(1) NOT NULL DEFAULT 1,
          `slika` VARCHAR(200) NOT NULL,
          PRIMARY KEY (`id`))
        ENGINE = InnoDB;


        -- -----------------------------------------------------
        -- Table `trgovina`.`posta`
        -- -----------------------------------------------------
        CREATE TABLE IF NOT EXISTS `trgovina`.`posta` (
          `postnaSt` INT NOT NULL,
          `imePoste` VARCHAR(100) NOT NULL,
          PRIMARY KEY (`postnaSt`))
        ENGINE = InnoDB;


        -- -----------------------------------------------------
        -- Table `trgovina`.`stranka`
        -- -----------------------------------------------------
        CREATE TABLE IF NOT EXISTS `trgovina`.`stranka` (
          `id` INT NOT NULL AUTO_INCREMENT,
          `ime` VARCHAR(45) NOT NULL,
          `priimek` VARCHAR(45) NOT NULL,
          `email` VARCHAR(45) NOT NULL,
          `geslo` VARCHAR(255) NOT NULL,
          `telSt` VARCHAR(45) NOT NULL,
          `aktiviran` TINYINT(1) NOT NULL DEFAULT 0,
          `naslov` VARCHAR(255) NOT NULL,
          `postnaSt` INT NOT NULL,
          `kodaAktivacije`VARCHAR(11),
          PRIMARY KEY (`id`),
          INDEX `fk_stranka_posta1_idx` (`postnaSt` ASC),
          CONSTRAINT `fk_stranka_posta1`
            FOREIGN KEY (`postnaSt`)
            REFERENCES `trgovina`.`posta` (`postnaSt`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION)
        ENGINE = InnoDB;


        -- -----------------------------------------------------
        -- Table `trgovina`.`narocilo`
        -- -----------------------------------------------------
        CREATE TABLE IF NOT EXISTS `trgovina`.`narocilo` (
          `id` INT NOT NULL AUTO_INCREMENT,
          `status` VARCHAR(45) NOT NULL,
          `stranka_id` INT NOT NULL,
          `datum` DATETIME NOT NULL,
          PRIMARY KEY (`id`),
          INDEX `fk_narocilo_stranka_idx` (`stranka_id` ASC),
          CONSTRAINT `fk_narocilo_stranka`
            FOREIGN KEY (`stranka_id`)
            REFERENCES `trgovina`.`stranka` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION)
        ENGINE = InnoDB;


        -- -----------------------------------------------------
        -- Table `trgovina`.`administrator`
        -- -----------------------------------------------------
        CREATE TABLE IF NOT EXISTS `trgovina`.`administrator` (
          `id` INT NOT NULL AUTO_INCREMENT,
          `ime` VARCHAR(45) NOT NULL,
          `priimek` VARCHAR(45) NOT NULL,
          `email` VARCHAR(45) NOT NULL,
          `geslo` VARCHAR(255) NOT NULL,
          PRIMARY KEY (`id`))
        ENGINE = InnoDB;


        -- -----------------------------------------------------
        -- Table `trgovina`.`prodajalec`
        -- -----------------------------------------------------
        CREATE TABLE IF NOT EXISTS `trgovina`.`prodajalec` (
          `id` INT NOT NULL AUTO_INCREMENT,
          `ime` VARCHAR(45) NOT NULL,
          `priimek` VARCHAR(45) NOT NULL,
          `email` VARCHAR(45) NOT NULL,
          `geslo` VARCHAR(255) NOT NULL,
          `aktiviran` TINYINT(1) NOT NULL DEFAULT 1,
          PRIMARY KEY (`id`))
        ENGINE = InnoDB;


        -- -----------------------------------------------------
        -- Table `trgovina`.`artikel_has_narocilo`
        -- -----------------------------------------------------
        CREATE TABLE IF NOT EXISTS `trgovina`.`artikel_has_narocilo` (
          `artikel_id` INT NOT NULL,
          `narocilo_id` INT NOT NULL,
          `stArtiklov` INT NOT NULL,
          INDEX `fk_artikel_has_narocilo_narocilo1_idx` (`narocilo_id` ASC),
          INDEX `fk_artikel_has_narocilo_artikel1_idx` (`artikel_id` ASC),
          PRIMARY KEY (`artikel_id`, `narocilo_id`),
          CONSTRAINT `fk_artikel_has_narocilo_artikel1`
            FOREIGN KEY (`artikel_id`)
            REFERENCES `trgovina`.`artikel` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
          CONSTRAINT `fk_artikel_has_narocilo_narocilo1`
            FOREIGN KEY (`narocilo_id`)
            REFERENCES `trgovina`.`narocilo` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION)
        ENGINE = InnoDB;


        -- -----------------------------------------------------
        -- Table `trgovina`.`ocena`
        -- -----------------------------------------------------
        CREATE TABLE IF NOT EXISTS `trgovina`.`ocena` (
          `id` INT NOT NULL AUTO_INCREMENT,
          `ocena` INT NOT NULL,
          `artikel_id` INT NOT NULL,
          `idStranke` INT NOT NULL,
          PRIMARY KEY (`id`),
          INDEX `fk_ocena_artikel1_idx` (`artikel_id` ASC),
          INDEX `fk_ocena_stranka1_idx` (`idStranke` ASC),
          CONSTRAINT `fk_ocena_artikel1`
            FOREIGN KEY (`artikel_id`)
            REFERENCES `trgovina`.`artikel` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
          CONSTRAINT `fk_ocena_stranka1`
            FOREIGN KEY (`idStranke`)
            REFERENCES `trgovina`.`stranka` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION)
        ENGINE = InnoDB;


        -- -----------------------------------------------------
        -- Table `trgovina`.`kosarica`
        -- -----------------------------------------------------
        CREATE TABLE IF NOT EXISTS `trgovina`.`kosarica` (
          `stranka_id` INT NOT NULL,
          `artikel_id` INT NOT NULL,
          `stArtiklov` VARCHAR(45) NOT NULL,
          PRIMARY KEY (`stranka_id`, `artikel_id`),
          INDEX `fk_stranka_has_artikel_artikel1_idx` (`artikel_id` ASC),
          INDEX `fk_stranka_has_artikel_stranka1_idx` (`stranka_id` ASC),
          CONSTRAINT `fk_stranka_has_artikel_stranka1`
            FOREIGN KEY (`stranka_id`)
            REFERENCES `trgovina`.`stranka` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
          CONSTRAINT `fk_stranka_has_artikel_artikel1`
            FOREIGN KEY (`artikel_id`)
            REFERENCES `trgovina`.`artikel` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION)
        ENGINE = InnoDB;
        
        
        -- -----------------------------------------------------
        -- Table `trgovina`.`dnevnikAdmin`
        -- -----------------------------------------------------
        CREATE TABLE IF NOT EXISTS `trgovina`.`dnevnikAdmin` (
          `id` INT NOT NULL AUTO_INCREMENT,
          `admin_id` INT NOT NULL,
          `datum` DATETIME NOT NULL,
          `aktivnost` VARCHAR(45) NOT NULL,
          `podrobnosti` VARCHAR(255) NOT NULL,
          PRIMARY KEY (`id`),
          INDEX `fk_dnevnikAdmin_administrator1_idx` (`admin_id` ASC),
          CONSTRAINT `fk_dnevnikAdmin_administrator1`
            FOREIGN KEY (`admin_id`)
            REFERENCES `trgovina`.`administrator` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION)
        ENGINE = InnoDB;
        

        -- -----------------------------------------------------
        -- Table `trgovina`.`dnevnikProdajalec`
        -- -----------------------------------------------------
        CREATE TABLE IF NOT EXISTS `trgovina`.`dnevnikProdajalec` (
          `id` INT NOT NULL AUTO_INCREMENT,
          `prodajalec_id` INT NOT NULL,
          `datum` DATETIME NOT NULL,
          `aktivnost` VARCHAR(45) NOT NULL,
          `podrobnosti` VARCHAR(255) NOT NULL,
          PRIMARY KEY (`id`),
          INDEX `fk_dnevnikProdajalec_prodajalec1_idx` (`prodajalec_id` ASC),
          CONSTRAINT `fk_dnevnikProdajalec_prodajalec1`
            FOREIGN KEY (`prodajalec_id`)
            REFERENCES `trgovina`.`prodajalec` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION)
        ENGINE = InnoDB;


        SET SQL_MODE=@OLD_SQL_MODE;
        SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
        SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
        ";
        return $sql;
    }
}
