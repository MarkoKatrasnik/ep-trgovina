<?php

require_once 'AbstractDB.php';

class ProdajalecDB extends AbstractDB {

    public static function insert(array $params) {
        return parent::modify("INSERT INTO prodajalec (ime, priimek, email, geslo, naslov, telSt) "
                        . " VALUES (:ime, :priimek, :email, :geslo, :naslov, :telSt)", $params);
    }

    public static function get(array $id) {
        $books = parent::query("SELECT id, naziv, opis, cena"
                        . " FROM artikel"
                        . " WHERE id = :id", $id);

        if (count($books) == 1) {
            return $books[0];
        } else {
            throw new InvalidArgumentException("Ni artikla s takim id");
        }
    }
    
    public static function getProdajalec(array $email) {
        $prodajalci = parent::query("SELECT id, email, geslo, aktiviran"
                        . " FROM prodajalec"
                        . " WHERE email = :email", $email);

        if (count($prodajalci) == 1) {
            return $prodajalci[0];
        } else {
            throw new InvalidArgumentException("Ni prodajalca s takim emailom");
        }
    }
    
    public static function getProfil(array $id) {
        $prodajalci = parent::query("SELECT *"
                        . " FROM prodajalec"
                        . " WHERE id = :id", $id);

        if (count($prodajalci) == 1) {
            return $prodajalci[0];
        } else {
            throw new InvalidArgumentException("Ni prodajalca s takim id-jem");
        }
    }
    
    public static function getStrankeList() {
        return parent::query("SELECT id, ime, priimek, aktiviran"
                        . " FROM stranka"
                        . " ORDER BY id ASC");
    }
    
    
    public static function dodajStranko(array $params) {
        return parent::modify("INSERT INTO stranka (ime, priimek, email, geslo, telSt, naslov, postnaSt, aktiviran) "
                        . " VALUES (:ime, :priimek, :email, :geslo, :telSt, :naslov, :postnaSt, :aktiviran)", $params);
    }
    
    public static function getStranka(array $id) {
        $stranke = parent::query("SELECT *"
                        . " FROM stranka"
                        . " WHERE id = :id", $id);

        if (count($stranke) == 1) {
            return $stranke[0];
        } else {
            throw new InvalidArgumentException("Ni uporabnika s takim emailom");
        }
    }
    
    public static function getAktivacija(array $id) {
        $stranke = parent::query("SELECT id, ime, priimek, email, aktiviran"
                        . " FROM stranka"
                        . " WHERE id = :id", $id);

        if (count($stranke) == 1) {
            return $stranke[0];
        } else {
            throw new InvalidArgumentException("Ni uporabnika s takim idjem");
        }
    }    
    
    public static function updateAktivacija(array $id) {
        $stranke = parent::query("SELECT id, ime, priimek, email, aktiviran"
                        . " FROM stranka"
                        . " WHERE id = :id", $id);

        if (count($stranke) == 1) {
            if(($stranke[0]["aktiviran"]) == 1){
                return parent::modify("UPDATE stranka SET aktiviran = 0"
                        . " WHERE id = :id", $id);
            }else{
                return parent::modify("UPDATE stranka SET aktiviran = 1"
                        . " WHERE id = :id", $id);
            }
            
        } else {
            throw new InvalidArgumentException("Ni uporabnika s takim idjem");
        }
    }
    
    
    public static function updateStrankaProfil(array $params) {
        return parent::modify("UPDATE stranka SET ime = :ime, priimek = :priimek, "
                        . "email = :email, telSt = :telSt, naslov = :naslov, postnaSt = :postnaSt"
                        . " WHERE id = :id", $params);
    }
    
    public static function insertNarocilo(array $params) {
        return parent::modify("INSERT INTO narocilo (status, stranka_id) "
                        . " VALUES (:status, :stranka_id)", $params);
    }
    
    public static function insertPovezava(array $params) {
        return parent::modify("INSERT INTO artikel_has_narocilo (artikel_id, narocilo_id, stArtiklov) "
                        . " VALUES (:artikel_id, :narocilo_id, :stArtiklov)", $params);
    }

    public static function getAll() {
        return parent::query("SELECT id, naziv, cena"
                        . " FROM artikel"
                        . " ORDER BY id ASC");
    }
    
    public static function updateProfil(array $params) {
        return parent::modify("UPDATE prodajalec SET ime = :ime, priimek = :priimek, "
                        . "email = :email"
                        . " WHERE id = :id", $params);
    }
    
    public static function updateGeslo(array $params) {
        return parent::modify("UPDATE prodajalec SET geslo = :geslo"
                        . " WHERE id = :id", $params);
    }
    
    public static function updateStrankaGeslo(array $params) {
        return parent::modify("UPDATE stranka SET geslo = :geslo"
                        . " WHERE id = :id", $params);
    }  
       
    public static function getAllwithURI(array $prefix) {
        return parent::query("SELECT id, author, title, price, year, "
                        . "          CONCAT(:prefix, id) as uri "
                        . "FROM book "
                        . "ORDER BY id ASC", $prefix);
    }
    
    public static function getNarocilo(array $stranka_id) {
        return parent::query("SELECT *"
                        . " FROM narocilo"
                        . " WHERE stranka_id = :stranka_id", $stranka_id);
    }
    
    public static function getNarociloPodrobnosti(array $id) {
        return parent::query("select narocilo.stranka_id, artikel_has_narocilo.narocilo_id, artikel.naziv, artikel.cena, artikel_has_narocilo.stArtiklov, narocilo.status FROM narocilo INNER JOIN artikel_has_narocilo ON narocilo.id = artikel_has_narocilo.narocilo_id INNER JOIN artikel ON artikel_has_narocilo.artikel_id = artikel.id WHERE narocilo.id = :id", $id);
    }
    
    public static function getArtikliList() {
        return parent::query("SELECT *"
                        . " FROM artikel"
                        . " ORDER BY id ASC");
    }   
    
    public static function dodajArtikel(array $params) {
        return parent::modify("INSERT INTO artikel (naziv, opis, cena, aktiviran, slika) "
                        . " VALUES (:naziv, :opis, :cena, :aktiviran, :slika2)", $params);
    }
    
    public static function updateArtikel(array $params) {
        return parent::modify("UPDATE artikel SET naziv = :naziv, opis = :opis, "
                        . "cena = :cena, slika = :slika2"
                        . " WHERE id = :id", $params);
    }
    
    public static function getArtikelOcena(array $id) {
        $artikli = parent::query("SELECT ocena, stOcen"
                        . " FROM artikel"
                        . " WHERE id = :id", $id);

        if (count($artikli) == 1) {
            return $artikli[0];
        } else {
            throw new InvalidArgumentException("Ni artikla");
        }
    }
    
    public static function posodobiOceno(array $params) {
        return parent::modify("UPDATE artikel SET ocena = :novaOcena, stOcen = :stOcen"
                        . " WHERE id = :id", $params);
    }
    
    public static function getAktivacijaArtikla(array $id) {
        $artikli = parent::query("SELECT id, naziv, opis, cena, aktiviran"
                        . " FROM artikel"
                        . " WHERE id = :id", $id);

        if (count($artikli) == 1) {
            return $artikli[0];
        } else {
            throw new InvalidArgumentException("Ni artikla s takim idjem");
        }
    } 
    
    public static function updateAktivacijaArtikla(array $id) {
        $artikli = parent::query("SELECT id, naziv, opis, cena, aktiviran"
                        . " FROM artikel"
                        . " WHERE id = :id", $id);

        if (count($artikli) == 1) {
            if(($artikli[0]["aktiviran"]) == 1){
                return parent::modify("UPDATE artikel SET aktiviran = 0"
                        . " WHERE id = :id", $id);
            }else{
                return parent::modify("UPDATE artikel SET aktiviran = 1"
                        . " WHERE id = :id", $id);
            }
            
        } else {
            throw new InvalidArgumentException("Ni artikla s takim idjem");
        }
    }
    
    public static function getOddanaNarocila() {
        return parent::query("SELECT stranka.id AS 'idStranke', stranka.ime AS 'imeStranke', stranka.priimek AS 'priimekStranke', stranka.aktiviran AS 'aktiviranStranke',"
                        . " artikel.id AS 'idArtikla', artikel.naziv AS 'nazivArtikla', artikel.aktiviran AS 'aktiviranArtikela',"
                        . " artikel_has_narocilo.stArtiklov AS 'ahnStArtiklov', artikel_has_narocilo.artikel_id AS 'ahnIdArtikla', artikel_has_narocilo.narocilo_id AS 'ahnIdNarocila',"
                        . " narocilo.id AS 'idNarocila', narocilo.stranka_id AS 'idStrankeNarocila', narocilo.status AS 'statusNarocila', narocilo.datum AS 'datumNarocila'"
                        . " FROM stranka, artikel, artikel_has_narocilo, narocilo"
                        . " WHERE (narocilo.status = 'Oddano')"
                        . " AND (narocilo.stranka_id = stranka.id)"
                        . " AND (narocilo.id = artikel_has_narocilo.narocilo_id)"
                        . " AND (artikel.id = artikel_has_narocilo.artikel_id) ORDER BY narocilo.datum DESC");
    }
    
    public static function updateNarociloPotrjeno(array $id) {
        return parent::modify("UPDATE narocilo SET status = 'Potrjeno' "
                        . " WHERE id = :id", $id);
    }
    
    public static function updateNarociloPreklicano(array $id) {
        return parent::modify("UPDATE narocilo SET status = 'Preklicano' "
                        . " WHERE id = :id", $id);
    }
    
    public static function getPotrjenaNarocila() {
        return parent::query("SELECT stranka.id AS 'idStranke', stranka.ime AS 'imeStranke', stranka.priimek AS 'priimekStranke', stranka.aktiviran AS 'aktiviranStranke',"
                        . " artikel.id AS 'idArtikla', artikel.naziv AS 'nazivArtikla', artikel.aktiviran AS 'aktiviranArtikela',"
                        . " artikel_has_narocilo.stArtiklov AS 'ahnStArtiklov', artikel_has_narocilo.artikel_id AS 'ahnIdArtikla', artikel_has_narocilo.narocilo_id AS 'ahnIdNarocila',"
                        . " narocilo.id AS 'idNarocila', narocilo.stranka_id AS 'idStrankeNarocila', narocilo.status AS 'statusNarocila', narocilo.datum AS 'datumNarocila'"
                        . " FROM stranka, artikel, artikel_has_narocilo, narocilo"
                        . " WHERE ((narocilo.status = 'Potrjeno') OR (narocilo.status = 'Stornirano'))"
                        . " AND (narocilo.stranka_id = stranka.id)"
                        . " AND (narocilo.id = artikel_has_narocilo.narocilo_id)"
                        . " AND (artikel.id = artikel_has_narocilo.artikel_id) ORDER BY narocilo.datum DESC");
    }    
    
    public static function updateNarociloStornirano(array $id) {
        return parent::modify("UPDATE narocilo SET status = 'Stornirano' "
                        . " WHERE id = :id", $id);
    }
    
    public static function insertDnevnik(array $params) {
        return parent::modify("INSERT INTO dnevnikProdajalec (prodajalec_id, datum, aktivnost, podrobnosti) "
                        . " VALUES (:prodajalec_id, NOW(), :aktivnost, :podrobnosti)", $params);
    }
    
    public static function getDnevnik(array $prodajalec_id) {
        return parent::query("SELECT *"
                        . " FROM dnevnikProdajalec"
                        . " WHERE prodajalec_id = :prodajalec_id", $prodajalec_id);
    }
}