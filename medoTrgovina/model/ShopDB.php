<?php

require_once 'AbstractDB.php';

class ShopDB extends AbstractDB {

    public static function insert(array $params) {
    }

    public static function get(array $id) {
        $artikli = parent::query("SELECT artikel.id, artikel.naziv, artikel.cena, artikel.opis, artikel.aktiviran, artikel.slika, AVG(ocena.ocena) as ocena"
                        . " FROM artikel INNER JOIN ocena ON artikel.id = ocena.artikel_id"
                        . " WHERE artikel.id = :id", $id);

        if (count($artikli) == 1) {
            return $artikli[0];
        } else {
            throw new InvalidArgumentException("Ta artikel ne obstaja.");
        }
    }

    public static function getAll() {
        return parent::query("SELECT id, naziv, cena"
                        . " FROM artikel WHERE aktiviran=1"
                        . " ORDER BY id ASC");
    }

    public static function getAllwithURI(array $prefix) {
        return parent::query("SELECT id, naziv, cena, "
                        . "          CONCAT(:prefix, id) as uri "
                        . "FROM artikel WHERE aktiviran=1 "
                        . "ORDER BY id ASC", $prefix);
    }

}
