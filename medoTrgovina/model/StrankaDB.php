<?php

require_once 'AbstractDB.php';

class StrankaDB extends AbstractDB {

    public static function insert(array $params) {
        return parent::modify("INSERT INTO stranka (ime, priimek, email, geslo, telSt, naslov, postnaSt, kodaAktivacije) "
                        . " VALUES (:ime, :priimek, :email, :geslo, :telSt, :naslov, :postnaSt, :kodaAktivacije)", $params);
    }

    public static function get(array $id) {
        $artikli = parent::query("SELECT id, naziv, opis, cena"
                        . " FROM artikel"
                        . " WHERE id = :id", $id);

        if (count($artikli) == 1) {
            return $artikli[0];
        } else {
            throw new InvalidArgumentException("Ni artikla s takim id");
        }
    }
    
    public static function getUser(array $email) {
        $stranke = parent::query("SELECT id, email, geslo, aktiviran"
                        . " FROM stranka"
                        . " WHERE email = :email", $email);

        if (count($stranke) == 1) {
            return $stranke[0];
        } else {
            throw new InvalidArgumentException("Ni uporabnika s takim emailom");
        }
    }
    
    public static function getProfil(array $id) {
        $stranke = parent::query("SELECT stranka.ime, stranka.priimek, stranka.email, stranka.telSt, stranka.naslov, stranka.postnaSt, posta.imePoste"
                        . " FROM stranka INNER JOIN posta ON stranka.postnaSt = posta.postnaSt"
                        . " WHERE stranka.id = :id", $id);

        if (count($stranke) == 1) {
            return $stranke[0];
        } else {
            throw new InvalidArgumentException("Ni uporabnika s takim id-jem");
        }
    }
    
        public static function getKodaAktivacije(array $id) {
        $stranke = parent::query("SELECT *"
                        . " FROM stranka"
                        . " WHERE id = :id", $id);

        if (count($stranke) == 1) {
            return $stranke[0];
        } else {
            throw new InvalidArgumentException("Ni uporabnika s takim id-jem");
        }
    }
    
    public static function insertNarocilo(array $params) {
        return parent::modify("INSERT INTO narocilo (status, stranka_id, datum) "
                        . " VALUES (:status, :stranka_id, NOW())", $params);
    }
    
    public static function insertPovezava(array $params) {
        return parent::modify("INSERT INTO artikel_has_narocilo (artikel_id, narocilo_id, stArtiklov) "
                        . " VALUES (:artikel_id, :narocilo_id, :stArtiklov)", $params);
    } 
    
    public static function dodajVKosarico(array $params) {
        return parent::modify("INSERT INTO kosarica (artikel_id, stranka_id, stArtiklov) "
                        . " VALUES (:artikel_id, :stranka_id, 1)", $params);
    }
    
    public static function dodajVKosaricoVec(array $params) {
        return parent::modify("INSERT INTO kosarica (artikel_id, stranka_id, stArtiklov) "
                        . " VALUES (:artikel_id, :stranka_id, :stArtiklov)", $params);
    }
    
    public static function getKosarica(array $params) {
        return parent::query("SELECT * FROM kosarica WHERE stranka_id = :stranka_id", $params);
    }
    
    
    public static function pridobiKosarico(array $params) {
        return parent::query("SELECT kosarica.stranka_id, kosarica.artikel_id, kosarica.stArtiklov, artikel.naziv, artikel.cena "
                . "FROM kosarica INNER JOIN artikel ON kosarica.artikel_id = artikel.id WHERE kosarica.stranka_id = :stranka_id", $params);
    }
    
    
    public static function posodobiKosarico(array $params) {
        return parent::modify("UPDATE kosarica SET stArtiklov = :stArtiklov WHERE artikel_id = :artikel_id AND stranka_id = :stranka_id", $params);
    }
    
    public static function preveriKosarico(array $params) {
        return parent::query("SELECT * FROM kosarica WHERE stranka_id = :stranka_id AND artikel_id = :artikel_id", $params);
    }
    
    public static function izprazniKosarico(array $id_stranke) {
        return parent::modify("DELETE FROM kosarica WHERE stranka_id = :stranka_id", $id_stranke);
    }
    
    public static function odstraniIzKosarice(array $id_stranke) {
        return parent::modify("DELETE FROM kosarica WHERE stranka_id = :stranka_id AND artikel_id = :artikel_id" , $id_stranke);
    }
    
    public static function getAll() {
        return parent::query("SELECT id, naziv, cena"
                        . " FROM artikel"
                        . " ORDER BY id ASC");
    }
    
    public static function updateProfil(array $params) {
        return parent::modify("UPDATE stranka SET ime = :ime, priimek = :priimek, "
                        . "email = :email, telSt = :telSt, naslov = :naslov, postnaSt = :postnaSt"
                        . " WHERE id = :id", $params);
    }

    
    public static function updateGeslo(array $params) {
        return parent::modify("UPDATE stranka SET geslo = :geslo"
                        . " WHERE id = :id", $params);
    }

    
    
    public static function getNarocilo(array $stranka_id) {
        return parent::query("SELECT *"
                        . " FROM narocilo"
                        . " WHERE stranka_id = :stranka_id ORDER BY datum DESC", $stranka_id);
    }
    
    public static function getNarociloPodrobnosti(array $id) {
        return parent::query("select narocilo.stranka_id, artikel_has_narocilo.narocilo_id, artikel.naziv, artikel.cena, "
                . "artikel_has_narocilo.stArtiklov, narocilo.status, narocilo.datum FROM narocilo INNER JOIN artikel_has_narocilo "
                . "ON narocilo.id = artikel_has_narocilo.narocilo_id INNER JOIN artikel ON artikel_has_narocilo.artikel_id = artikel.id "
                . "WHERE narocilo.id = :id", $id);
    }
    
    public static function getArtikelOcena(array $id) {
        $ocene = parent::query("SELECT ocena, stOcen"
                        . " FROM artikel"
                        . " WHERE id = :id", $id);

        if (count($ocene) == 1) {
            return $ocene[0];
        } else {
            throw new InvalidArgumentException("No such book");
        }
    }
    
    public static function dodajOceno(array $params) {
        return parent::modify("INSERT INTO ocena (ocena, idStranke, artikel_id)"
                        . " VALUES (:ocena, :idStranke, :id)", $params);
    }
    
    public static function stOcen(array $params) {
        $stOcen = parent::query("SELECT COUNT(ocena) as stevilo"
                        . " FROM ocena"
                        . " WHERE idStranke = :idStranke AND artikel_id = :id", $params);
        return $stOcen[0];
    }
    
    public static function postaneAktiviran(array $registriranec) {
        return parent::modify("UPDATE stranka SET aktiviran = 1, kodaAktivacije = NULL"
                        . " WHERE id = :id", $registriranec);
    }
}