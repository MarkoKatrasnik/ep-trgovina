<?php

require_once("dodajTabele.php");
require_once("dodajVnoseVTabele.php");

class DBInit {
    
    private static $host = "localhost";
    private static $user = "root";
    private static $password = "ep";
    private static $schema = "trgovina";
    private static $instance = null;

    private function __construct() {
        
    }

    private function __clone() {
        
    }

    /**
     * Vrne instanco razreda PDO, ki vsebuje povezavo na bazo.
     * 
     * @return PDO Instanca razreda PDO 
     */
    public static function getInstance() {
        if (!self::$instance) {
            try {
                $config = "mysql:host=" . self::$host
                    . ";dbname=" . self::$schema;
                $options = array(
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                    PDO::ATTR_PERSISTENT => true,
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
                );

                self::$instance = new PDO($config, self::$user, self::$password, $options);
                self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (Exception $e) {
                self::$instance = new PDO("mysql:host=".self::$host, self::$user, self::$password);
                self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                self::$instance->query("
                            SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
                            SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
                            SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

                            -- -----------------------------------------------------
                            -- Schema trgovina
                            -- -----------------------------------------------------

                            -- -----------------------------------------------------
                            -- Schema trgovina
                            -- -----------------------------------------------------
                            CREATE SCHEMA IF NOT EXISTS `trgovina` DEFAULT CHARACTER SET utf8 ;
                            USE `trgovina` ;
                            ");
                $sql1 = dodajTabele::sqlPoizvedba();
                self::$instance->query($sql1);
                $sql2 = dodajVnoseVTabele::sqlPoizvedba();
                self::$instance->query($sql2);

                //echo ("Trgovini smo uspešno dodali tabele\n");

            }
        }
        
        return self::$instance;
    }

}